<!DOCTYPE html>
<html lang="zxx">
<style>
    @media(max-width:767px) {
        .webView {
            display: none;
        }

        .webContent {
            display: none;
        }

        #title {
            font-size: 20px !important;
        }
    }

    @media(min-width:768px) {
        .mobileNav {
            display: none;
        }

        .mobileContent {
            display: none;
        }
    }

    .color-red {
        color: red;
    }

    .nav-item .nav-menu li a {
        color: black !important;
    }

    .line-bottom {
        height: 4px;
        background-color: #008D50;
        width: 70%;
        margin-left: auto;
        margin-right: auto;
    }

    .line-bottom-premium {
        height: 4px;
        background-color: goldenrod;
        width: 40%;
        margin-left: auto;
        margin-right: auto;
    }

    .login-text {
        font-family: 'Roboto' !important;
        font-size: 16px;
        color: #272727;
    }

    .footer-stay {
        position: absolute;
        bottom: 0;
        width: 100%;
    }

    .text-center {
        font-family: 'Roboto' !important;
        font-size: 20px;
        font-weight: bold;
        color: #3A595D;
    }

    .pointer {
        cursor: pointer;
    }

    .fixed-top-custom {
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 1030;
    }

    .pointer {
        cursor: pointer;
    }

    .dropdown-list:hover {
        background-color: #D9FCFF;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        width: max-content;
        right: 0;
        /*z-index: 1;*/
    }

    .dropdown-content a {
        color: black;
        padding: 6px;
        text-align: left;
        text-decoration: none;
        display: block;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        padding: 12px 16px;
        z-index: 1;
        font-size: small;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown-content a:hover {
        color: #27AE60;
    }

    .hide {
        display: none;
    }

    /* #submit{
    width: 247px;
    height: 54px;
    border-radius: 30px;
    align-content: center;
    background-color: #27AE60;
    color: white;
    margin-left: auto; 
    margin-right: auto;
} */


    html * {
        font-family: 'Roboto' !important;
    }

    i {
        font-family: "FontAwesome" !important;

        display: inline-block;
        font-style: normal;
        font-variant: normal;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
    }

    .package-card {
        background-color: white;
        box-shadow: 0px 4px 10px #0000004D;
        height: 100%;
        border-radius: 4px;
    }

    .title-regular {
        font-size: 16px;
        color: #00AFB6;
        text-align: center;
        font-weight: bold;
    }

    .title-premium {
        font-size: 16px;
        color: #FFA700;
        text-align: center;
        font-weight: bold;
    }

    .btn-regular {
        background-color: #00AFB6 !important;
        border: 1px solid #00AFB6 !important;
        color: white !important;
    }

    .btn-premium {
        background-color: #FFA700 !important;
        border: 1px solid #FFA700 !important;
        color: white !important;
    }

    .btn-grey {
        background-color: #C4C4C4 !important;
        border: 1px solid #C4C4C4 !important;
        color: white !important;
    }

    .section-box-custom {
        background: #FFFFFF 0% 0% no-repeat padding-box;
        border: 0.5px solid #CBCBCB;
        border-radius: 6px;
    }

    .section-title-custom {
        font-size: 18px;
        text-align: left !important;
        font-weight: bold;
    }

    .custom-label {
        font-size: 12px;
        margin-bottom: 16px;
    }

    .shelflife {
        visibility: hidden;
        position: absolute;
        z-index: 1;
        border-radius: 5px;
    }

    .show-shelflife {
        visibility: visible;
    }

    .guide {
        visibility: hidden;
        position: absolute;
        z-index: 1;
        border-radius: 5px;
    }

    .show-guide {
        visibility: visible;
    }

    .tooltip-custom {
        position: relative;
        display: inline-block;
        color: #868686;
        font-size: 14px;
        float: left !important;
        margin-right: 3px;
    }

    .tooltip-custom .tooltiptext-custom {
        visibility: hidden;
        width: auto;
        max-width: 400px;
        min-width: 250px;
        background-color: white;
        color: black;
        /* text-align: center; */
        border-radius: 6px;
        padding: 16px;
        border: 1px solid #8b8b8b;

        /* Position the tooltip-custom */
        position: absolute;
        z-index: 1;
        top: -5px;
        left: 105%;
    }

    .tooltip-custom:hover .tooltiptext-custom {
        visibility: visible;
    }

    a:hover {
        color: #007bff !important;
    }

    .hide {
        display: none;
    }

    .white-14 {
        color: white;
        font-size: 14px;
    }

    .black-bold {
        color: black;
        font-weight: bold;
        font-size: 14px;
    }

    .gray-bold {
        color: lightgray;
        font-weight: bold;
    }

    .overflow-hidden {
        overflow: hidden;
    }
</style>

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Waste 2 Wealth</title>
    <link rel="icon" type="image/png" sizes="16x16" href="img/website-logo.png">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body style="position: relative; height: auto; min-height: 100vh;">
    <!-- Page Preloder -->
    <!-- <div id="preloder">
        <div class="loader"></div>
    </div> -->

    <div style="background-color: #EDC647; height: 500px; width: 100%; position: absolute;">

    </div>
    <div class="mobileNav fixed-top shadow-1" style="background-color: white">

        <!--Navbar-->
        <nav class="navbar navbar-light light-blue lighten-4">
            <!-- Navbar brand -->
            <button onclick="toggleSideMenu()" class="navbar-toggler toggler-example mr-2" type="button" style="height: 38px;"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button>
            <div class="logo" style="margin-right: 56px;">
                <a href="./">
                    <img src="img/website-logo.png" style="width: 76px;">
                </a>
            </div>
            <div class="row">
                <div class="user hide">
                    <div class="dropdown my-auto mr-3">
                        <!-- <button class="btn" style="display: flex; color: white; font-size: 14px; padding: 0"><i style="font-size: 40px; margin-left: 5px; color: gray; background-color: white; border-radius: 50%;" class="fa fa-user-circle"></i></button> -->

                        <!-- <img id="previewProfile6" src="./img/avatar.svg" style="height: 38px; width: 38px; border-radius: 50%; object-fit: cover;"> -->

                        <div class="dropdown-content" style="border-radius: 4px; background-color: white; padding: 0">
                            <div style="padding: 10px;">
                                <div onclick="window.location.href='./my-ads.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                    My Ads
                                </div>
                                <div onclick="window.location.href='./profile.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                    My Account
                                </div>
                                <div onclick="window.location.href='./token-list.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                    Buy Token
                                </div>
                            </div>
                            <div style="background-color: #F2F2F2;">
                                <div class="d-flex" style="padding: 10px 10px 10px 20px;">
                                    <div class="my-auto" style="color: #049D19; font-size: 14px;">
                                        <span id="userBalanceToken1">0</span> Tokens
                                    </div>
                                    <div class="ml-5">
                                        <button onclick="window.location.href='./logout.php'" class="btn" style="background-color: white; border: 1px solid #CBCBCB; color: #5482FF;">Logout</button>
                                    </div>
                                </div>
                                <!-- <div onclick="window.location.href='./logout.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
    Logout
</div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <button onclick="toggleSideMenu()" class="navbar-toggler toggler-example mr-2" type="button" style="height: 38px;"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button> -->
                <!-- <button class="navbar-toggler toggler-example mr-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button> -->
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                <ul class="navbar-nav">
                    <li class="nav-item justify-content-center">
                        <div class="d-flex my-auto">

                            <div class="guest my-auto">

                                <a href="./signin.html"><button class="btn nav-title">Sign
                                        In</button></a>
                                <span style="font-size: 14px; color: darkgoldenrod">or</span>
                                <div class="dropdown">
                                    <button class="btn nav-title" style="display: flex;">Register
                                        <i style="font-size: 10px; margin-top: 4px; margin-left: 5px" class="fa fa-chevron-down"></i></button>
                                    <div class="dropdown-content" style="border-radius: 4px; padding: 10px; background-color: white">
                                        <div onclick="window.location.href='register-individu.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Individual
                                        </div>
                                        <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                        </div>
                                        <div onclick="window.location.href='register-company.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Company
                                        </div>
                                        <!-- <a href="#">English</a>
    <a href="#">Malay</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="./setup-ads.html"><button class="btn btn-primary mr-1" style="font-size: 12px; font-weight: bold;">Create
                                Ads</button></a>
                    </li>

                    <li class="nav-item justify-content-center">

                    </li>
                    <li class="nav-item active justify-content-center">
                        <a class="nav-link" onclick="window.location.href='./supply.php'" href="#">Available Product <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item justify-content-center">
                        <a class="nav-link" onclick="window.location.href='./demand.php'" href="#">Wanted Product</a>
                    </li>
                    <li class="nav-item justify-content-center">
                        <a class="nav-link contactUs" href="#"> Contact Us</a>
                    </li>
                </ul>
                <!-- Links -->

            </div>
            <!-- Collapsible content -->

        </nav>
        <!--/.Navbar-->
        <div class="sideMenu hide d-flex" style="position: fixed; z-index: 100; width: 100%;">
            <div style="height: 100vh; width: 230px; min-width: 230px; overflow-y: auto; background-color: white; box-shadow: 0px 4px 8px 0px;">
                <!-- head -->
                <div style="width: 100%; background-color: #3A595D; padding: 12px;">
                    <div class="float-right">
                        <i onclick="toggleSideMenu()" class="fa fa-times" style="font-size: 20px; color: white; -webkit-text-stroke: 2px; -webkit-text-stroke-color: #3A595D; opacity: 0.6;"></i>
                    </div>
                    <div style="height: 24px; width: 100%;"></div>
                    <div class="user hide">
                        <div>
                            <img id="previewProfile1" src="./img/avatar.svg" style="height: 50px; width: 50px; border-radius: 50%; object-fit: cover;">
                        </div>
                        <div class="mt-2 white-14 d-flex justify-content-between">
                            <span class="navUserName"></span><span class="white-14" style="font-size: 14px;"><span class="navUserToken"></span> Token(s)</span>
                        </div>
                        <div class="navUserType gray-bold" style="font-size: 12px;"></div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                    </div>
                    <div class="guest">
                        <div onclick="window.location.href=`./signin.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                            Sign In
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div class="white-14 d-flex justify-content-between myaccount" style="padding-top: 5px; padding-bottom: 5px;">
                            Register <span><i class="myaccount-icon fa fa-chevron-down"></i></span>
                        </div>
                        <div class="myaccount-child hide" style="font-size: 14px; color: white;">
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./register-individu.html`" class="mb-1 mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                As Individual
                            </div>
                            <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./register-company.html`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                As Company
                            </div>
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                    </div>
                    <div onclick="window.location.href=`./setup-ads.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                        Create Ads <span><i class="fa fa-plus-circle"></i></span>
                    </div>
                    <div class="user hide">
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div onclick="window.location.href=`./my-ads.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                            My Ads
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div class="white-14 d-flex justify-content-between myaccount" style="padding-top: 5px; padding-bottom: 5px;">
                            My Account <span><i class="myaccount-icon fa fa-chevron-down"></i></span>
                        </div>
                        <div class="myaccount-child hide" style="font-size: 14px; color: white;">
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./profile.html`" class="mb-1 mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                Profile
                            </div>
                            <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./wallet.html`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                My Wallet
                            </div>
                            <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./logout.php`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                Logout
                            </div>
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div onclick="window.location.href=`./token-list.php`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                            Buy Token
                        </div>
                    </div>
                </div>
                <div style="padding: 12px;">
                    <div onclick="window.location.href=`./supply.php`" class="d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                        Available Product
                    </div>
                    <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.3;"></div>
                    <div onclick="window.location.href=`./demand.php`" class="d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                        Wanted Product
                    </div>
                    <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.3;"></div>
                    <div class="d-flex justify-content-between contactUs" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                        Contact Us
                    </div>
                </div>
            </div>
            <div onclick="toggleSideMenu()" style="background-color: black; height: 100vh; width: 100%; opacity: 0.5;">

            </div>
        </div>
    </div>
    <div class="webView fixed-top">
        <div class="d-flex flex-fill">
            <div class="shadow-1" style="background-color: white; width: 100%; padding-top: 5px; padding-bottom: 5px;">
                <div class="col-md-10 offset-md-1 " style="padding-top: 5px; padding-bottom: 5px;">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex justify-content-between">
                            <div class="logo" style="width: 76px;">
                                <a href="./">
                                    <img src="img/website-logo.png">
                                </a>
                            </div>
                            <div class="d-flex ml-3">
                                <div onclick="window.location.href='./supply.php'" class="nav-title my-auto ml-3 pointer">
                                    Available Product
                                </div>
                                <div onclick="window.location.href='./demand.php'" class="nav-title my-auto ml-3 pointer">
                                    Wanted Product
                                </div>
                                <div class="nav-title my-auto ml-3 pointer contactUs">
                                    Contact Us
                                </div>
                            </div>
                        </div>
                        <div class="d-flex my-auto">
                            <div class="guest my-auto">
                                <a href="./signin.html"><button class="btn nav-title">Sign
                                        In</button></a>
                                <span style="font-size: 14px; color: darkgoldenrod">or</span>
                                <div class="dropdown">
                                    <button class="btn nav-title" style="display: flex;">Register
                                        <i style="font-size: 10px; margin-top: 4px; margin-left: 5px" class="fa fa-chevron-down"></i></button>
                                    <div class="dropdown-content" style="border-radius: 4px; padding: 10px; background-color: white">
                                        <div onclick="window.location.href='register-individu.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Individual
                                        </div>
                                        <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                        </div>
                                        <div onclick="window.location.href='register-company.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Company
                                        </div>
                                        <!-- <a href="#">English</a>
                                    <a href="#">Malay</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="user hide">
                                <div class="dropdown my-auto mr-3">
                                    <!-- <button class="btn" style="display: flex; color: white; font-size: 14px; padding: 0"><i style="font-size: 40px; margin-left: 5px; color: gray; background-color: white; border-radius: 50%;" class="fa fa-user-circle"></i></button> -->
                                    <div>
                                        <img id="previewProfile" src="./img/avatar.svg" style="height: 42px; width: 42px; border-radius: 50%; object-fit: cover;">
                                    </div>
                                    <div class="dropdown-content" style="border-radius: 4px; background-color: white; padding: 0">
                                        <div style="padding: 10px;">
                                            <div onclick="window.location.href='./my-ads.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                My Ads
                                            </div>
                                            <div onclick="window.location.href='./profile.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                My Account
                                            </div>
                                            <div onclick="window.location.href='./token-list.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Buy Token
                                            </div>
                                            <!-- <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                            </div> -->
                                        </div>
                                        <div style="background-color: #F2F2F2;">
                                            <div class="d-flex" style="padding: 10px 10px 10px 20px;">
                                                <div class="my-auto" style="color: #049D19; font-size: 14px;">
                                                    <span id="userBalanceToken">0</span> Tokens
                                                </div>
                                                <div class="ml-5">
                                                    <button onclick="window.location.href='./logout.php'" class="btn" style="background-color: white; border: 1px solid #CBCBCB; color: #5482FF;">Logout</button>
                                                </div>
                                            </div>
                                            <!-- <div onclick="window.location.href='./logout.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Logout
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <a href="./login.html"><button class="btn"
                                style="color: white; font-size: 14px;">Register</button></a> -->
                            <a href="./setup-ads.html"><button class="btn btn-primary" style="font-size: 14px; font-weight: bold;padding:10px">Create
                                    Ads</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="package-view">
        <div style="padding-top: 140px; padding-bottom: 100px;">
            <div class="row" style="margin: 0;">
                <div class="col-md-8 offset-md-2" style="text-align: center;">
                    <div id="title" style="font-size: 40px;">
                        <!-- Be a supplier and sell your product -->
                    </div>
                    <div style="margin-top: 48px;">
                        <div class="section-box-custom" style="width: 100%; padding: 24px; margin-bottom: 20px;">
                            <div class="section-title-custom" style="margin-bottom: 24px;">
                                Ad Details
                            </div>
                            <div class="details-section text-left">

                                <div class="custom-label">
                                    Date of Submission
                                </div>
                                <div style="margin-bottom: 16px;">
                                    <input id="submissionDate" type="text" class="form-control" value="<?php echo date('d/m/Y') ?>" style="width: fit-content;" disabled>
                                </div>
                                <div class="isWaste hide">
                                    <div class="custom-label">
                                        Name of Material <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="wasteName" type="text" class="form-control" placeholder="Please state">
                                    </div>
                                    <div class="custom-label">
                                        State of Material <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <select id="wasteStateMaterial" class="form-control">
                                            <option disabled selected>Please choose</option>
                                            <option value="solid">Solid</option>
                                            <option value="liquid">Liquid</option>
                                            <option value="gaseous">Gaseous</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="isTechnology hide">
                                    <div class="custom-label">
                                        Name of Technology <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="technologyName" type="text" class="form-control" placeholder="Please state">
                                    </div>
                                </div>
                                <div class="isService hide">
                                    <div class="custom-label">
                                        Name of service <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="serviceName" type="text" class="form-control" placeholder="Please state">
                                    </div>
                                </div>
                                <div class="custom-label">
                                    Category <span class="color-red">*</span>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8" style="margin-bottom: 16px;">
                                        <!-- <select id="category" class="form-control">
                                        <option value="">Please choose</option>
                                    </select> -->
                                        <div style="margin-top: auto; margin-bottom: auto; width: 100%;">
                                            <select id="parentCategory" onchange="checkStatusChild()" class="form-control">

                                            </select>
                                        </div>
                                        <!-- <input id="category" type="text" class="form-control"> -->
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="d-flex">
                                            <div>
                                                <button style="display: none; font-size: 10px;" class="btn btn-primary" id="addNewLevel" onclick="subListingCategory()"> Add Level</button>
                                            </div>
                                            <div class="ml-3">
                                                <button class="btn" style="font-size: 10px; border: 1px solid #007bff; color: #007bff;" onclick="ResetCategory()">Reset Level</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="isWaste hide">
                                    <div class="custom-label">
                                        Product Season <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <select id="wasteSeason" class="form-control">
                                            <option disabled selected>Please choose</option>
                                            <option value="continous">Continous</option>
                                            <option value="one-time">One-Time</option>
                                            <option value="seasonal">Seasonal</option>
                                        </select>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-label">
                                                Quantity
                                            </div>
                                            <div style="margin-bottom: 16px;">
                                                <input id="wasteQuantity" type="number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="custom-label">
                                                Unit
                                            </div>
                                            <div style="margin-bottom: 16px;">
                                                <input id="wasteUnit" type="text" class="form-control" placeholder="eg: kg">
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="isTechnology hide">
                                    <div class="custom-label">
                                        Target Industry <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <!-- <select id="technologyTargetIndustry" class="form-control">
                                            <option value="">Please choose</option>
                                        </select> -->
                                        <input id="technologyTargetIndustry" type="text" class="form-control">
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-label">
                                            Selling Price
                                        </div>
                                        <div style="margin-bottom: 16px;">
                                            <input id="sellingPrice" type="number" class="form-control">
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-6 col-sm-4 col-md-4">
                                        <div class="custom-label isAvailable hide">
                                            Selling Price <span class="color-red">*</span>
                                        </div>
                                        <div class="custom-label isWanted hide">
                                            Wanted Price <span class="color-red">*</span>
                                        </div>
                                        <div style="margin-bottom: 16px;">
                                            <input id="sellingPrice" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="isWaste hide" style="font-size: 24px; margin-top: 32px;">/</div>
                                    <div class="col-5 col-sm-4 col-md-4 isWaste hide">
                                        <div class="custom-label">
                                            Quantity <span class="color-red">*</span>
                                        </div>
                                        <div style="margin-bottom: 16px;">
                                            <input id="wasteQuantity" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-3 col-md-3 isWaste hide">
                                        <div class="custom-label">
                                            Unit <span class="color-red">*</span>
                                        </div>
                                        <div style="margin-bottom: 16px;">
                                            <input id="wasteUnit" type="text" class="form-control" placeholder="eg: kg">
                                        </div>
                                    </div>
                                </div>
                                <div class="isWaste hide">
                                    <div class="custom-label">Shelflife <span class="color-red">*</span>
                                        <i class="fa fa-info-circle tooltip-custom">
                                            <span class="tooltiptext-custom">
                                                <div style="font-size: 16px; color: #8b8b8b;">Degradation period</div>
                                                <div style="margin-top: 20px; font-size: 16px;">The length of time for which an item remains usable, fit for consumption, or saleable</div>
                                            </span>
                                        </i>
                                    </div>
                                    <!-- <div id="shelflife" class="shelflife" style="width: 400px;border: 1px solid #8b8b8b; background-color: white; padding: 10px; margin-left: 50px;">
                                        
                                    </div> -->
                                    <div style="margin-bottom: 16px;">
                                        <select id="wasteShelflife" class="form-control">
                                            <option disabled selected>Please choose</option>
                                            <option value="within 3 months">Within 3 months</option>
                                            <option value="within 6 months">Within 6 months</option>
                                            <option value="within 1 year">Within 1 year</option>
                                            <option value="never">Never</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-label">
                                            Suggested Supplier/Buyer <span class="color-red">*</span>
                                        </div>
                                        <div style="margin-bottom: 16px;">
                                            <input id="suggestedCustomer" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 1px; width: 100%; background-color: #cecece; margin-top: 24px; margin-bottom: 24px;"></div>
                            <div class="location-section text-left">
                                <div>
                                    <div class="custom-label">
                                        Location
                                    </div>
                                    <div style="font-size: 12px; font-style: italic; color: #707070; margin-bottom: 16px;">
                                        Step 1 - Enter your material location <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="line1" type="text" class="form-control" placeholder="Line 1">
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="line2" type="text" class="form-control" placeholder="Line 2">
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="city" type="text" class="form-control" placeholder="City">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="margin-bottom: 16px;">
                                            <select id="state" class="form-control">
                                                <option disabled selected>State</option>
                                                <option value="johor">Johor</option>
                                                <option value="kedah">Kedah</option>
                                                <option value="kelantan">Kelantan</option>
                                                <option value="melaka">Melaka</option>
                                                <option value="negeri sembilan">Negeri Sembilan</option>
                                                <option value="pahang">Pahang</option>
                                                <option value="perak">Perak</option>
                                                <option value="perlis">Perlis</option>
                                                <option value="pulau pinang">Pulau Pinang</option>
                                                <option value="sabah">Sabah</option>
                                                <option value="sarawak">Sarawak</option>
                                                <option value="selangor">Selangor</option>
                                                <option value="selangor">Terengganu</option>
                                                <option value="wilayah persekutuan malaysia">Wilayah Persekutuan Malaysia</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6" style="margin-bottom: 16px;">
                                            <input id="postalCode" type="text" class="form-control" placeholder="Postal code">
                                        </div>
                                    </div>
                                    <div style="font-size: 12px; font-style: italic; color: #707070; margin-bottom: 16px;">
                                        Step 2 - Enter your latitude and longitude <span class="color-red">*</span>
                                        <i class="fa fa-info-circle tooltip-custom" style="color: #868686; font-size: 14px;">
                                            <span class="tooltiptext-custom">
                                                <div style="font-size: 16px; color: #8b8b8b;">Quick tips to find your latitude & longitude:</div>
                                                <div style="margin-top: 20px; font-size: 16px;">1. Open Google Maps, and enter your full address.</div>
                                                <div style="margin-top: 20px; font-size: 16px;">2. Press enter to find your location.</div>
                                                <div style="margin-top: 20px; font-size: 16px;">3. Click on your exact location.</div>
                                                <div style="font-style: italic; color: #ff7600; font-size: 16px;">&nbsp;&nbsp;&nbsp;(But, do not click on the red icon)</div>
                                                <div style="margin-top: 20px; font-size: 16px;">4. By clicking at your exact location, a pop-up card &nbsp;&nbsp;&nbsp;&nbsp;will be shown with your exact location address, &nbsp;&nbsp;&nbsp;&nbsp;latitude and longitude.</div>
                                                <div style="margin-top: 20px; font-size: 16px;">5. Copy the latitude and longitude shown in the &nbsp;&nbsp;&nbsp;&nbsp;pop-up. (Eg: 1.545428, 103.612502)</div>
                                                <div style="margin-top: 20px; font-size: 16px;">6. Lastly, paste it here after you have copied it.</div>
                                            </span>
                                        </i>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" style="margin-bottom: 16px;">
                                            <input id="latitude" type="text" class="form-control" placeholder="Latitude">
                                        </div>
                                        <div class="col-md-6" style="margin-bottom: 16px;">
                                            <input id="longitude" type="text" class="form-control" placeholder="Longitude">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 1px; width: 100%; background-color: #cecece; margin-top: 24px; margin-bottom: 24px;"></div>
                            <div class="additional-section text-left">
                                <div class="isWaste hide">
                                    <div class="custom-label">
                                        Transportation Mode <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <select id="wasteTransportationMode" class="form-control">
                                            <option disabled selected>Please choose</option>
                                            <option value="pick up by customer">Pick up by customer</option>
                                            <option value="delivered by seller">Delivered by seller</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="isTechnology hide">
                                    <div class="custom-label">
                                        Website <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="technologyWeb" type="text" class="form-control" placeholder="Please state">
                                    </div>
                                </div>
                                <div class="isService hide">
                                    <div class="custom-label">
                                        Website <span class="color-red">*</span>
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="serviceWeb" type="text" class="form-control" placeholder="Please state">
                                    </div>
                                </div>
                                <div class="custom-label">
                                    Additional Information <span class="color-red">*</span>
                                </div>
                                <div style="margin-bottom: 16px;">
                                    <textarea id="additionalInformation" class="form-control" rows="5" style="resize: none;" placeholder="Write your additional information..."></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="section-box-custom" style="width: 100%; padding: 24px; margin-bottom: 20px;">
                            <div class="section-title-custom" style="margin-bottom: 16px;">
                                Upload Photos <span class="color-red">*</span>
                            </div>
                            <div class="d-flex" style="height: 200px; border: 1px solid #c4c4c4; border-radius: 4px; background-color: #ffffff 0% 0% no-repeat padding-box;">
                                <div class="my-auto" style="width: 100%">
                                    <div class="d-flex justify-content-around">
                                        <!-- <i class="fa fa-upload" style="font-size: 40px; color: #707070;"></i>
                                        <div class="mt-2" style="font-size: 14px; color: #707070;">
                                            Drag & drop images here or <span id="browseFile" class="pointer" style="color: #007fff;">Browse</span>
                                        </div> -->
                                        <div class="my-auto">
                                            <img id="previewProfile1" class="icon-center pointer" style="width: 100px; height: 100px; border-radius: 5%" src="img/addphoto.svg">
                                            <div>
                                                <button id="removeBtn1" class="btn hide">Remove</button>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <img id="previewProfile2" class="icon-center pointer" style="width: 100px; height: 100px; border-radius: 5%" src="img/addphoto.svg">
                                            <div>
                                                <button id="removeBtn2" class="btn hide">Remove</button>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <img id="previewProfile3" class="icon-center pointer" style="width: 100px; height: 100px; border-radius: 5%" src="img/addphoto.svg">
                                            <div>
                                                <button id="removeBtn3" class="btn hide">Remove</button>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <img id="previewProfile4" class="icon-center pointer" style="width: 100px; height: 100px; border-radius: 5%" src="img/addphoto.svg">
                                            <div>
                                                <button id="removeBtn4" class="btn hide">Remove</button>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <img id="previewProfile5" class="icon-center pointer" style="width: 100px; height: 100px; border-radius: 5%" src="img/addphoto.svg">
                                            <div>
                                                <button id="removeBtn5" class="btn hide">Remove</button>
                                            </div>
                                        </div>

                                        <input id="imageFile1" type="file" onchange="readURL(this, 'previewProfile1')" hidden>
                                        <input id="imageFile2" type="file" onchange="readURL(this, 'previewProfile2')" hidden>
                                        <input id="imageFile3" type="file" onchange="readURL(this, 'previewProfile3')" hidden>
                                        <input id="imageFile4" type="file" onchange="readURL(this, 'previewProfile4')" hidden>
                                        <input id="imageFile5" type="file" onchange="readURL(this, 'previewProfile5')" hidden>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 24px; font-size: 12px; color: #707070; text-align: left;">
                                You can upload between 1 to 5 photos. Supported image file type are jpeg, jpg and png
                                only. Each image must be in 1MB. The first photo uploaded will be the cover photo.
                            </div>
                        </div>

                        <div class="section-box-custom" style="width: 100%; padding: 24px; margin-bottom: 20px;">
                            <div class="section-title-custom" style="margin-bottom: 16px;">
                                Contact Details
                            </div>
                            <div class="text-left">
                                <div class="custom-label">
                                    Name <span class="color-red">*</span>
                                </div>
                                <div style="margin-bottom: 16px;">
                                    <input id="contactName" type="text" class="form-control">
                                </div>
                                <div class="custom-label">
                                    Email Address <span class="color-red">*</span>
                                </div>
                                <div style="margin-bottom: 16px;">
                                    <input id="contactEmail" type="email" class="form-control">
                                </div>
                                <div class="custom-label">
                                    Telephone Number <span class="color-red">*</span>
                                </div>
                                <div style="margin-bottom: 16px;">
                                    <input id="contactPhone" type="text" class="form-control">
                                </div>
                                <!-- <div id="isCompany" class="hide">
                                    <div class="custom-label">
                                        Person In-charge (PIC)
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="picName" type="text" class="form-control">
                                    </div>
                                    <div class="custom-label">
                                        Telephone Number (PIC)
                                    </div>
                                    <div style="margin-bottom: 16px;">
                                        <input id="picPhone" type="text" class="form-control">
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="section-box-custom" style="width: 100%; padding: 24px;">
                            <div class="section-title-custom">
                                Your Package
                            </div>
                            <div id="selectedPackage" style="margin-top: 24px;">

                            </div>
                        </div>

                        <div style="margin-top: 72px;">
                            <div class="d-flex flex-fill justify-content-start">
                                <div>
                                    <input id="check" type="checkbox">
                                </div>
                                <div class="ml-2" style="font-size: 14px; color: #707070;">
                                    <label for="check">
                                        I agree that I have read and accepted
                                    </label>
                                    <a href="./assets/tnc/Terms and Conditions of W2W.pdf" target="_blank">Terms and Conditions</a>
                                    <!-- <span class="pointer" style="color: #2D9CDB;" onclick="`./assets/tnc/tnc.docx`">Terms and Conditions.</span> -->
                                </div>
                            </div>
                        </div>

                        <div style="margin-top: 40px; margin-bottom: 40px;">
                            <div class="d-flex">
                                <div>
                                    <!-- <button id="previewAds" class="btn" style="height: 50px; width: 160px; border-radius: 10px; background-color: white; border: 1px solid #007FFF; color: #007FFF;">Preview Ad</button> -->
                                </div>
                                <div class="ml-3">
                                    <button id="submitAds" class="btn" style="height: 50px; width: 160px; border-radius: 10px; background-color: #007FFF; border: 1px solid #007FFF; color: white;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer begin -->
    <footer id="footer" class="footer-section" style="background-color: #4B4B4B; position: absolute; bottom: 0; width: 100%">
        <div class="container">
            <div class="row" style="padding-top: 5px;">
                <div class="col-lg-2">
                    <a onclick="$(`#contactUsModal`).modal()" style="color: white; cursor: pointer;">Contact Us</a>
                </div>
                <div class="col-lg-4">
                    <a href="./assets/tnc/Terms and Conditions of W2W.pdf" target="_blank" style="color: white;">Terms and Conditions</a>
                </div>
                <div class="col-lg-5">
                    <p style="color: white;">
                        Copyright &copy; Waste 2 Wealth Website. All rights reserved
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- Sign in modal -->
    <div class="modal fade" id="modalSuccess">
        <div class="modal-dialog modal-dialog-centered">

            <div class="row" style="padding: 20px; width: 350px; position: absolute; left: 50%; top: 30%; margin-left: -150px;margin-top: -150px;">
                <div class="modal-content">
                    <div class="card" style="padding: 20px; width: 350px;">
                        <div class="login-form" style="text-align: center;">
                            <div>
                                <img src="./img/checkijau.svg">
                            </div>
                            <div style="color: #27AE60; font-size: 28px;">
                                Successful!
                            </div>
                            <div style="margin-top: 37px; font-size: 20px; font-weight: bold;">
                                Your form is submitted.
                            </div>
                            <div style="margin-top: 16px; color: #707070; font-size: 18px;">
                                Please be patient.<br>Your form will be approved within 3 days, and you will be notified upon its approval.
                            </div>
                            <div style="margin-top: 40px;">
                                <button id="continue" class="btn" style="width: 155px; height: 46px; background-color: #27AE60; border: 1px solid #27AE60; color: white;">Continue</button>
                            </div>
                            <div>
                                <!-- <a href="#" class="forget-pass float-right login-text" style="color: #D91F1F; padding-top: 20px;">Forgot pasword?</a> -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--preview ad modal-->
    <div class="modal fade" id="modalPreviewAds" role="dialog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-body" style="padding: 30px;">
                    <div class="container" style="margin-bottom: 30px; margin-left: 15px;">
                        <div class="row">
                            <div class="col-md-5">
                                <img src="img/hensemboi.jpg">
                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-3">
                                        <img src="img/hensemboi.jpg">
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/hensemboi.jpg">
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/hensemboi.jpg">
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/hensemboi.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="material_nameM" style="color: gray; margin-bottom: 10px;">Plastic Pellets</div>
                                <!-- <label style="color: white; background-color: green; border-radius: 10px; padding: 5px;">Available</label> -->
                                <div class="badge badge-success">Available</div>
                                <div id="priceM" style="margin-top: 20px; font-size: 26px; font-weight: bold;">RM 200.00
                                </div>
                                <div style="color: gray;">100 unit</div>
                            </div>
                            <div class="col-md-3">
                                <div style="font-size: 12px;">For product pricing, customization or other inquiries:
                                </div>
                                <br>
                                <button class="btn" style="background-color: #FF5700; color: white; width: 240px; border-radius: 50px;">Make an offer</button>
                            </div>
                        </div>
                    </div>
                    <div class="container" style="padding: 30px;">
                        <div class="">
                            <div style="background-color: #F2F2F2; padding: 10px; margin: 10px;">Material Information
                            </div>
                            <div style="padding: 20px;">
                                <div class="row detail-spacing">
                                    <div class="col-12 col-md-4">
                                        <label>Date of submission</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="dateM">20/12/2020</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Material Status</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div>Available</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Name of Material</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="nameM">Plastic Pellets</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>State of Material</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="material_stateM">Solid</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Category</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="categoryM">Plastics</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Product Season</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="seasonM">One-time</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Transportation Mode</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="transportM">Pick up by customer</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Details</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="descriptionM">We can supply minimal amount</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Quantity</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="quantityunitM">80 mton</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Shelf Life/ Degradation period</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="shelflifeM">Never</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Price of Material</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="priceM2">Free</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Waste (location 1)</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="locationM">Pasir Gudang</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>State</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="stateM">Johor</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Waste (location 2)</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="locationM2">Kulai</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>State</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="stateM2">Johor</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Supplier</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="nameM2">Siti Fatimah Saad</div>
                                    </div>
                                </div>
                                <div class="row detail-spacing">
                                    <div class="col-md-4">
                                        <label>Company Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="company_nameM">-</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;">
                        <button class="btn" style="background-color: white; border: 1px solid #27AE60; color: #27AE60; border-radius: 10px; padding-left: 30px; padding-right: 30px;" data-dismiss="modal">Edit</button>
                        <button class="btn" style="background-color: #27AE60; color: white; border-radius: 10px; padding-left: 20px; padding-right: 20px;" data-toggle="modal" data-dismiss="modal" data-target="#myModal8">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- contact us Modal -->
    <div class="modal fade" id="contactUsModal" tabindex="-1" role="dialog" aria-labelledby="contactUsModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div style="text-align: center; font-size: 30px; margin-top: 10px;">Contact Us</div>
                    <div style="padding: 60px;">
                        <div id="successMsg" class="hide">
                            <div style="font-size: 14px; color: #269635;">
                                Thank you for your message. It has been sent!
                            </div>
                            <br>
                            <div>
                                <button onclick="$('#contactUsModal').modal('hide')" class="btn btn-block" style="background-color: #27AE60; border: 1px solid #27AE60; color: white;">Okay</button>
                            </div>
                        </div>
                        <div id="defaultContact">
                            <div>
                                <input id="contactUsName" type="text" class="form-control" placeholder="Name…">
                            </div>
                            <br>
                            <div>
                                <input id="contactUsEmail" type="email" class="form-control" placeholder="someone@example.com">
                            </div>
                            <br>
                            <div>
                                <select name="contactUsRegarding" id="contactUsRegarding" class="form-control">
                                    <option value="nothing">Regarding...</option>
                                    <option value="support">Support</option>
                                    <option value="report">Report</option>
                                </select>
                            </div>
                            <br>
                            <div>
                                <textarea id="contactUsMessage" class="form-control" cols="20" rows="7"></textarea>
                            </div>
                            <br>
                            <div>
                                <button id="sendContactUs" class="btn btn-block" style="background-color: #27AE60; border: 1px solid #27AE60; color: white;">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- not fill Modal -->
    <div class="modal fade" id="validationModal" tabindex="-1" role="dialog" aria-labelledby="validationModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div style="padding: 60px; text-align: center; color: red;">
                        Please fill in all the forms
                    </div>
                    <div>
                        <button data-toggle="modal" data-dismiss="modal" data-target="#validationModal" class="btn btn-primary float-right">OKAY</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>

</html>


<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery.dd.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
<script src="js/login.js"></script>

<script>
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    (function($) {
        $.fn.inputFilter3 = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                var value = parseFloat(this.value);
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }

                if (value > 100000) {
                    this.value = "";
                    this.value = $temp1;
                } else {
                    $temp1 = this.value;
                }
            });
        };
    }(jQuery));

    $(window).resize(function() {
        console.log('resize called');
        var width = $(window).width();
        var height = $(window).height();
        var newHeight = height - 60;

        $('.sideMenu').children('div').css('height', newHeight);

        if (width <= 768) {
            // $('.sideMenu').removeClass('hide');
        } else {
            $('.sideMenu').addClass('hide');
        }
    }).resize()

    $(document).ready(function() {

        $("#postalCode").inputFilter3(function(value) {
            return /^\d*$/.test(value); // Allow digits only, using a RegExp
        });

        $('#removeBtn1').on('click', function() {
            $('#removeBtn1').addClass('hide');
            $('#previewProfile1').attr('src', 'img/addphoto.svg').css('width', '100px').css('height', '100px').css('border-radius', '5%');
            $('#imageFile1').val('');
        });

        $('#removeBtn2').on('click', function() {
            $('#removeBtn2').addClass('hide');
            $('#previewProfile2').attr('src', 'img/addphoto.svg').css('width', '100px').css('height', '100px').css('border-radius', '5%');
            $('#imageFile2').val('');
        });

        $('#removeBtn3').on('click', function() {
            $('#removeBtn3').addClass('hide');
            $('#previewProfile3').attr('src', 'img/addphoto.svg').css('width', '100px').css('height', '100px').css('border-radius', '5%');
            $('#imageFile3').val('');
        });

        $('#removeBtn4').on('click', function() {
            $('#removeBtn4').addClass('hide');
            $('#previewProfile4').attr('src', 'img/addphoto.svg').css('width', '100px').css('height', '100px').css('border-radius', '5%');
            $('#imageFile4').val('');
        });

        $('#removeBtn5').on('click', function() {
            $('#removeBtn5').addClass('hide');
            $('#previewProfile5').attr('src', 'img/addphoto.svg').css('width', '100px').css('height', '100px').css('border-radius', '5%');
            $('#imageFile5').val('');
        });

        $adsType = '';
        $adsCategory = '';
        $packageId = '';

        $listCategory = [];
        $fullList = [];
        var mainCategory = '';
        var nextChildCategory = '';
        $parentBody1 = $('#parentCategory');
        // $childBody1 = $('#levelCategory');


        $user = null;
        $('#previewAds').attr('disabled', true).css('cursor', 'default');
        $('#submitAds').attr('disabled', true).css('cursor', 'default');

        getUser();
        getSettings();
        getPackageDetails($packageId);

        // $('#browseFile').on('click', function() {
        //     $('#imageFile').trigger('click');
        // });

        $('#previewProfile1').on('click', function() {
            $('#imageFile1').trigger('click');
        });

        $('#previewProfile2').on('click', function() {
            $('#imageFile2').trigger('click');
        });

        $('#previewProfile3').on('click', function() {
            $('#imageFile3').trigger('click');
        });

        $('#previewProfile4').on('click', function() {
            $('#imageFile4').trigger('click');
        });

        $('#previewProfile5').on('click', function() {
            $('#imageFile5').trigger('click');
        });

        $("#shelflifepop").on('click', function() {
            if ($('#shelflife').hasClass('show-shelflife')) {
                $('#shelflife').removeClass('show-shelflife');
            } else {
                $('#shelflife').addClass('show-shelflife');
            }
        });

        $("#guidePop").on('click', function() {
            if ($('.guide').hasClass('show-guide')) {
                $('.guide').removeClass('show-guide');
            } else {
                $('.guide').addClass('show-guide');
            }
        });

        $('#check').on('click', function() {
            var checked = this.checked;
            if (checked == false) {
                $('#previewAds').attr('disabled', true).css('cursor', 'default');
                $('#submitAds').attr('disabled', true).css('cursor', 'default');
            } else {
                $('#previewAds').attr('disabled', false).css('cursor', 'pointer');
                $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
            }
        });


        $('#previewAds').on('click', function() {
            $('#modalPreviewAds').modal();
        });

        $('#submitAds').on('click', function() {
            $('#previewAds').attr('disabled', true).css('cursor', 'default');
            $('#submitAds').attr('disabled', true).css('cursor', 'default');
            var userType = $user['user_type'];
            if ($adsCategory == 'waste') {
                submitWaste(userType);
            } else if ($adsCategory == 'technology') {
                submitTechnology(userType);
            } else if ($adsCategory == 'service') {
                submitService(userType);
            }
        });

        $('#continue').on('click', function() {
            window.location.href = './';
        });

        $('.contactUs').on('click', function() {
            drawContactUs($user);
            $('#contactUsModal').modal();
        });
        $('#sendContactUs').on('click', function() {
            var name = '';
            var email = '';
            var category = '';
            var message = '';
            name = $('#contactUsName').val();
            email = $('#contactUsEmail').val();
            category = $('#contactUsRegarding').val();
            message = $('#contactUsMessage').val();
            if (name != '' && email != '' && category != 'nothing' && message != '') {
                $postData = {
                    'name': name,
                    'email': email,
                    'category': category,
                    'description': message,
                };
                postContactUs($postData);
            }
        });
        $("#contactUsModal").on("hidden.bs.modal", function() {
            $('#contactUsName').val('');
            $('#contactUsEmail').val('');
            $('#contactUsRegarding').val('nothing');
            $('#contactUsMessage').val('');
            $('#successMsg').addClass('hide');
            $('#defaultContact').removeClass('hide');
            $('#sendContactUs').attr('disabled', false);
        });

        $('#postalCode').inputFilter(function(value) {
            return /^\d*$/.test(value); //digit only
        });

        $('#sellingPrice').inputFilter(function(value) {
            return /^-?\d*\.?\d*$/.test(value); //allow empty
        });

        $('#wasteQuantity').inputFilter(function(value) {
            // return /^-?\d+\.?\d*$/.test(value); // not allow empty
            return /^-?\d*\.?\d*$/.test(value); //allow empty
        });

        $('#contactPhone').inputFilter(function(value) {
            // return /^-?\d+\.?\d*$/.test(value);
            return /^\d*$/.test(value); // Allow digits only, using a RegExp
        });

        $('#latitude').inputFilter(function(value) {
            // return /^-?\d+\.?\d*$/.test(value);
            return /^-?\d*\.?\d*$/.test(value); //allow empty
        });

        $('#longitude').inputFilter(function(value) {
            return /^-?\d*\.?\d*$/.test(value); //allow empty
        });

        $('.myaccount').on('click', function() {
            $('.myaccount-child').toggleClass('hide');

            if ($('.myaccount-icon').hasClass('fa-chevron-down')) {
                $('.myaccount-icon').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            } else {
                $('.myaccount-icon').addClass('fa-chevron-down').removeClass('fa-chevron-up');
            }
        });
    });

    function postContactUs(postData) {
        $('#sendContactUs').attr('disabled', true);
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/enquiry',
            method: 'POST',
            data: postData,
        }).done(function(response) {
            $('#successMsg').removeClass('hide');
            $('#defaultContact').addClass('hide');
            console.log(response);
        });
    }



    function getUserDetails(id) {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/user/detail?user_id=' + id,
        }).done(function(response) {
            $('#login').attr('disabled', false);
            if (response['status'] == 'success') {
                localStorage.setItem('w2wUser', JSON.stringify(response['value']));
                document.cookie = 'w2wLogged=true';
                // window.location.reload();
                // $('#modalSignIn').modal('hide');
                $('#modalSuccess').modal();
                // getUser();
            }
        });
    }

    function getTokenBalance() {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/token/mytoken?userid=' + $user['user_id'],
        }).done(function(response) {
            console.log(response);
            var data = response['value'];
            $('#userBalanceToken').html(data['tokenbalance']);
            $('#userBalanceToken1').html(data['tokenbalance']);
            $('.navUserToken').html(data['tokenbalance']);
        });
    }

    function getUser() {
        $user = JSON.parse(localStorage.getItem('w2wUser'));
        console.log($user);
        if ($user != null) {
            drawContactUs($user);
            var profilePicture = $user['profilepicture'];
            var extension = profilePicture.substr(profilePicture.length - 4);
            // if (extension.indexOf('png') >= 1 || extension.indexOf('jpg') >= 1 || extension.indexOf('jpeg') >= 1) {
            $('#previewProfile').attr('src', profilePicture);
            $('#previewProfile1').attr('src', profilePicture);
            $('#previewProfile6').attr('src', profilePicture);

            if ($user['user_type'] != 'company') {
                $('.navUserName').html($user['user_fname']).css('text-transform', 'capitalize');
            } else {
                $('.navUserName').html($user['companyname']).css('text-transform', 'capitalize');
            }
            $('.navUserType').html($user['user_type']).css('text-transform', 'capitalize');
            // }
            if ($user['balancetoken'] != null) {
                $('#userBalanceToken').html($user['balancetoken']);
                $('#userBalanceToken1').html($user['balancetoken']);
                $('.navUserToken').html($user['balancetoken']);
                getTokenBalance();
            } else {
                $('#userBalanceToken').html('0');
                $('#userBalanceToken1').html('0');
                $('.navUserToken').html('0');
            }
            $('.guest').addClass('hide');
            $('.user').removeClass('hide');

            if ($user['user_type'] == 'company') {
                $('#contactName').val($user['companyname']);
                $('#isCompany').removeClass('hide');
            } else {
                $('#contactName').val($user['user_fname'] + ' ' + $user['user_lname']);
                $('#isCompany').addClass('hide');
            }

            $('#contactEmail').val($user['user_email']);
            $('#contactPhone').val($user['user_contact']);
        }
    }

    function drawContactUs(data) {
        if (data != '' && data != null) {
            if (data['user_type'] == 'company') {
                $('#contactUsName').val(data['companyname']);
            } else {
                $('#contactUsName').val(data['user_fname'] + ' ' + data['user_lname']);
            }
            $('#contactUsEmail').val(data['user_email']);
        }
    }

    function getSettings() {
        var settings = JSON.parse(localStorage.getItem('w2wCreateAds'));
        console.log(settings);
        $adsType = settings['type'];
        $adsCategory = settings['category'];
        $packageId = settings['packageId'];
        listingSubCategory($adsCategory);
        if ($adsType == 'wanted') {
            $('.isWanted').removeClass('hide');
            $('#title').html('Make a demand or request for your product');
        } else {
            $('.isAvailable').removeClass('hide');
            $('#title').html('Be a supplier or sell your product');
        }
        if ($adsCategory == 'waste') {
            if ($('.isWaste').hasClass('hide')) {
                $('.isWaste').removeClass('hide');
            }
        } else if ($adsCategory == 'technology') {
            if ($('.isTechnology').hasClass('hide')) {
                $('.isTechnology').removeClass('hide');
            }
        } else if ($adsCategory == 'service') {
            if ($('.isService').hasClass('hide')) {
                $('.isService').removeClass('hide');
            }
        }
    }

    function getPackageDetails(id) {
        console.log('requesting');
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/packages/detail?package_id=' + id,
        }).done(function(response) {
            console.log(response);
            // $packageBody.html('');
            if (response['status'] == 'success') {
                if (response['value'].length != 0) {
                    $allPackages = response['value'];

                    var package = response['value'][0];

                    if (package['premiumlist'] == 'regular') {
                        drawPackage('regular', package);
                    } else {
                        drawPackage('premium', package);
                    }
                }
            }
        });
    }

    function categoryDropdownList() {
        $.ajax({
            url: "https://codeviable.com/w2w2/public/category/sub?id=65",
            type: "GET",
        }).done(function(response) {
            var tr = '<option disabled selected value="">Please choose</option>';

            $.each(response, function(i, data) {
                tr += '<option style="text-transform:capitalize;">' + data.name + '</option>';
            })
            $('#category').html(tr);
            $('#category2').html(tr);
        })
    }

    function readURL(input, idImg) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#' + idImg)
                    .attr('src', e.target.result)
                    .css('object-fit', 'cover')
                    .width(100)
                    .height(150);
            };
            reader.readAsDataURL(input.files[0]);

            if (idImg === 'previewProfile1') {
                $('#removeBtn1').removeClass('hide');
            } else if (idImg === 'previewProfile2') {
                $('#removeBtn2').removeClass('hide');
            } else if (idImg === 'previewProfile3') {
                $('#removeBtn3').removeClass('hide');
            } else if (idImg === 'previewProfile4') {
                $('#removeBtn4').removeClass('hide');
            } else if (idImg === 'previewProfile5') {
                $('#removeBtn5').removeClass('hide');
            }
        }
    }

    function drawPackage(type, data) {
        $packageBody = $('#selectedPackage');
        var titleClass = '';
        var buttonClass = '';

        if (type == 'regular') {
            titleClass = 'title-regular';
            // buttonClass = 'btn-regular';
        } else if (type == 'premium') {
            titleClass = 'title-premium';
            // buttonClass = 'btn-premium';
        }

        var buttonClass = 'btn-grey';

        var div = '';
        div += '<div class="col-md-3" style="padding: 0;">';
        div += '<div class="d-flex flex-column justify-content-between section-box-custom" style="border-radius: 4px;">';
        div += '<div class="d-flex flex-column justify-content-start" style="height: 100%; width: 100%;padding: 36px;">';
        div += '<div class="' + titleClass + '">';
        div += data['package_name'];
        div += '</div>';
        div += '<div style="margin-top: 20px; font-size: 12px; color: #868686;">';
        div += 'Featured Ads';
        div += '</div>';
        div += '<div style="height: 1px; background-color: lightgray; width: 100%;">';
        div += '</div>';
        div += '<div style="font-size: 12px; color: #868686;">';
        div += 'Ads Live for <span style="font-weight: bold;">' + data['package_duration'] + '</span>';
        div += '</div>';
        if (type == 'premium') {
            div += '<div style="height: 1px; background-color: lightgray; width: 100%;"></div>';
            div += '<div style="font-size: 12px; color: #868686;">';
            div += 'Highlighted Ad';
            div += '</div>';
        }
        div += '</div>';
        div += '<div class="d-flex flex-column justify-content-end" style = "height: 100%; width: 100%;padding: 24px;" >';
        div += '<div style="font-size: 20px; color: #868686; font-weight: bold;">';
        div += data['package_price'] + ' Tokens';
        div += '</div>';
        div += '<div style="margin-top: 40px;">';
        div += '<button data-id=' + data['package_id'] + ' class="selectPackage btn btn-block ' + buttonClass + '" disabled style="cursor: default;">Select</button>';
        div += '</div>';
        div += '</div>';
        div += '</div>';
        div += '</div>';

        $packageBody.html(div);
    }

    function submitWaste(userType) {
        console.log(userType);
        console.log('submit waste');


        var date = $('#submissionDate').val();
        var wasteName = $('#wasteName').val();
        var wasteStateMaterial = $('#wasteStateMaterial').val();
        var category = $('#parentCategory option:selected').text();

        var wasteSeason = $('#wasteSeason').val();
        var wasteQuantity = $('#wasteQuantity').val();
        var wasteUnit = $('#wasteUnit').val();
        var sellingPrice = $('#sellingPrice').val();
        var wasteShelflife = $('#wasteShelflife').val();
        var line1 = $('#line1').val();
        var line2 = $('#line2').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var postalCode = $('#postalCode').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var wasteTransportationMode = $('#wasteTransportationMode').val();
        var additionalInformation = $('#additionalInformation').val();
        var image1 = $('#imageFile1').prop('files');
        var image2 = $('#imageFile2').prop('files');
        var image3 = $('#imageFile3').prop('files');
        var image4 = $('#imageFile4').prop('files');
        var image5 = $('#imageFile5').prop('files');
        var contactName = $('#contactName').val();
        var contactEmail = $('#contactEmail').val();
        var contactPhone = $('#contactPhone').val();
        // var picName = $('#picName').val();
        // var picPhone = $('#picPhone').val();
        var suggestedCustomer = $('#suggestedCustomer').val();
        var address = line1 + ' ' + line2 + ' ' + state + ' ' + city;

        if (date != '' && wasteName != '' && wasteStateMaterial != '' && category != '' && wasteSeason != '' && wasteQuantity != '' && sellingPrice != '' && wasteUnit != '' && wasteShelflife != '' && line1 != '' && state != '' && city != '' && postalCode != '' && latitude != '' && longitude != '' && wasteTransportationMode != '' && additionalInformation != '' && suggestedCustomer != '' && contactName != '' && contactEmail != '' && contactPhone != '') {
            if (image1[0] != null || image2[0] != null || image3[0] != null || image4[0] != null || image5[0] != null) {
                var formData = new FormData();
                formData.append('product_date', date);
                formData.append('product_name', wasteName);
                formData.append('product_material', wasteStateMaterial);
                formData.append('product_category', category);
                formData.append('product_season', wasteSeason);
                formData.append('product_quantity', wasteQuantity);
                formData.append('product_price', sellingPrice);
                formData.append('unit', wasteUnit);
                formData.append('shellife', wasteShelflife);
                formData.append('product_location[]', address);
                formData.append('product_location[]', "");
                formData.append('product_location[]', "");
                formData.append('city[]', city);
                formData.append('city[]', "");
                formData.append('city[]', "");
                formData.append('product_state[]', state);
                formData.append('product_state[]', "null");
                formData.append('product_state[]', "null");
                formData.append('postalcode[]', postalCode);
                formData.append('postalcode[]', "");
                formData.append('postalcode[]', "");
                formData.append('latitud[]', latitude);
                formData.append('latitud[]', "");
                formData.append('latitud[]', "");
                formData.append('longitud[]', longitude);
                formData.append('longitud[]', "");
                formData.append('longitud[]', "");
                formData.append('product_transport', wasteTransportationMode);
                formData.append('product_description', additionalInformation);
                formData.append('suggestcustomer', suggestedCustomer);
                if (image1[0] != null) {
                    formData.append('product_image[]', image1[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image2[0] != null) {
                    formData.append('product_image[]', image2[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image3[0] != null) {
                    formData.append('product_image[]', image3[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image4[0] != null) {
                    formData.append('product_image[]', image4[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image5[0] != null) {
                    formData.append('product_image[]', image5[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                formData.append('name', contactName);
                formData.append('email', contactEmail);
                formData.append('contact', contactPhone);
                formData.append('user_id', $user['user_id']);
                formData.append('package_id', $packageId);
                formData.append('mainstatus', $adsType);
                formData.append('maincategory', $adsCategory);

                postAds(formData);
            } else {
                $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
                $('#validationModal').modal();
            }
        } else {
            $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
            $('#validationModal').modal();
        }

    }

    function submitTechnology(userType) {
        console.log(userType);
        console.log('submit technology');

        var date = $('#submissionDate').val();
        var technologyName = $('#technologyName').val();
        var category = $('#parentCategory option:selected').text();

        var technologyTargetIndustry = $('#technologyTargetIndustry').val();
        var sellingPrice = $('#sellingPrice').val();
        var line1 = $('#line1').val();
        var line2 = $('#line2').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var postalCode = $('#postalCode').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var technologyWeb = $('#technologyWeb').val();
        var additionalInformation = $('#additionalInformation').val();
        var image1 = $('#imageFile1').prop('files');
        var image2 = $('#imageFile2').prop('files');
        var image3 = $('#imageFile3').prop('files');
        var image4 = $('#imageFile4').prop('files');
        var image5 = $('#imageFile5').prop('files');
        var contactName = $('#contactName').val();
        var contactEmail = $('#contactEmail').val();
        var contactPhone = $('#contactPhone').val();
        var picName = $('#picName').val();
        var picPhone = $('#picPhone').val();
        var suggestedCustomer = $('#suggestedCustomer').val();

        var address = line1 + ' ' + line2 + ' ' + state + ' ' + city;

        if (date != '' && technologyName != '' && category != '' && technologyTargetIndustry != '' && sellingPrice != '' && line1 != '' && state != '' && city != '' && postalCode != '' && latitude != '' && longitude != '' && technologyWeb != '' && additionalInformation != '' && suggestedCustomer != '' && contactName != '' && contactEmail != '' && contactPhone != '') {
            if (image1[0] != null || image2[0] != null || image3[0] != null || image4[0] != null || image5[0] != null) {
                var formData = new FormData();
                formData.append('product_date', date);
                formData.append('product_name', technologyName);
                formData.append('product_category', category);
                formData.append('product_target', technologyTargetIndustry);
                formData.append('product_price', sellingPrice);
                formData.append('product_location[]', address);
                formData.append('product_location[]', "");
                formData.append('product_location[]', "");
                formData.append('city[]', city);
                formData.append('city[]', "");
                formData.append('city[]', "");
                formData.append('product_state[]', state);
                formData.append('product_state[]', "");
                formData.append('product_state[]', "");
                formData.append('postalcode[]', postalCode);
                formData.append('postalcode[]', "");
                formData.append('postalcode[]', "");
                formData.append('latitud[]', latitude);
                formData.append('latitud[]', "");
                formData.append('latitud[]', "");
                formData.append('longitud[]', longitude);
                formData.append('longitud[]', "");
                formData.append('longitud[]', "");
                formData.append('website', technologyWeb);
                formData.append('product_description', additionalInformation);
                formData.append('suggestcustomer', suggestedCustomer);
                if (image1[0] != null) {
                    formData.append('product_image[]', image1[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image2[0] != null) {
                    formData.append('product_image[]', image2[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image3[0] != null) {
                    formData.append('product_image[]', image3[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image4[0] != null) {
                    formData.append('product_image[]', image4[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image5[0] != null) {
                    formData.append('product_image[]', image5[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                formData.append('name', contactName);
                formData.append('company_email', contactEmail);
                formData.append('contact', contactPhone);
                formData.append('user_id', $user['user_id']);
                formData.append('package_id', $packageId);
                formData.append('mainstatus', $adsType);
                formData.append('maincategory', $adsCategory);
                formData.append('tagging[]', "['']");

                postAds(formData);
            } else {
                $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
                $('#validationModal').modal();
            }
        } else {
            $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
            $('#validationModal').modal();
        }
    }

    function submitService(userType) {
        console.log(userType);
        console.log('submit service');

        var date = $('#submissionDate').val();
        var serviceName = $('#serviceName').val();
        var category = $('#parentCategory option:selected').text();
        var sellingPrice = $('#sellingPrice').val();
        var line1 = $('#line1').val();
        var line2 = $('#line2').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var postalCode = $('#postalCode').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var serviceWeb = $('#serviceWeb').val();
        var additionalInformation = $('#additionalInformation').val();
        var image1 = $('#imageFile1').prop('files');
        var image2 = $('#imageFile2').prop('files');
        var image3 = $('#imageFile3').prop('files');
        var image4 = $('#imageFile4').prop('files');
        var image5 = $('#imageFile5').prop('files');
        var contactName = $('#contactName').val();
        var contactEmail = $('#contactEmail').val();
        var contactPhone = $('#contactPhone').val();
        var picName = $('#picName').val();
        var picPhone = $('#picPhone').val();
        var suggestedCustomer = $('#suggestedCustomer').val();

        var address = line1 + ' ' + line2 + ' ' + state + ' ' + city;

        if (date != '' && serviceName != '' && category != '' && sellingPrice != '' && line1 != '' && state != '' && city != '' && postalCode != '' && latitude != '' && longitude != '' && serviceWeb != '' && additionalInformation != '' && suggestedCustomer != '' && contactName != '' && contactEmail != '' && contactPhone != '') {
            if (image1[0] != null || image2[0] != null || image3[0] != null || image4[0] != null || image5[0] != null) {
                var formData = new FormData();
                formData.append('product_date', date);
                formData.append('product_name', serviceName);
                formData.append('product_category', category);
                formData.append('product_price', sellingPrice);
                formData.append('product_location[]', address);
                formData.append('product_location[]', "");
                formData.append('product_location[]', "");
                formData.append('city[]', city);
                formData.append('city[]', "");
                formData.append('city[]', "");
                formData.append('product_state[]', state);
                formData.append('product_state[]', "");
                formData.append('product_state[]', "");
                formData.append('postalcode[]', postalCode);
                formData.append('postalcode[]', "");
                formData.append('postalcode[]', "");
                formData.append('latitud[]', latitude);
                formData.append('latitud[]', "");
                formData.append('latitud[]', "");
                formData.append('longitud[]', longitude);
                formData.append('longitud[]', "");
                formData.append('longitud[]', "");
                formData.append('website', serviceWeb);
                formData.append('product_description', additionalInformation);
                formData.append('suggestcustomer', suggestedCustomer);
                if (image1[0] != null) {
                    formData.append('product_image[]', image1[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image2[0] != null) {
                    formData.append('product_image[]', image2[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image3[0] != null) {
                    formData.append('product_image[]', image3[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image4[0] != null) {
                    formData.append('product_image[]', image4[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                if (image5[0] != null) {
                    formData.append('product_image[]', image5[0]);
                } else {
                    formData.append('product_image[]', '');
                }
                formData.append('name', contactName);
                formData.append('company_email', contactEmail);
                formData.append('contact', contactPhone);
                formData.append('user_id', $user['user_id']);
                formData.append('package_id', $packageId);
                formData.append('mainstatus', $adsType);
                formData.append('maincategory', $adsCategory);
                formData.append('tagging[]', "['']");

                postAds(formData);
            } else {
                $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
                $('#validationModal').modal();
            }
        } else {
            $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
            $('#validationModal').modal();
        }
    }

    function postAds(postData) {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/product/add',
            method: 'POST',
            data: postData,
            processData: false,
            contentType: false,
        }).done(function(response) {
            console.log(response);
            $('#previewAds').attr('disabled', false).css('cursor', 'pointer');
            $('#submitAds').attr('disabled', false).css('cursor', 'pointer');
            if (response['status'] == 'success') {
                getUserDetails($user['user_id']);
            }
        });
    }

    function drawParentList(data, type) {
        var div = '';

        div += '<option value=' + data['id'] + '>' + data['name'] + '</option>';
        mainCategory = data['maincategory']
        $parentBody1.append(div);

    }

    function listingSubCategory(type) {
        $fullList = []
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/category/list?maincategory=' + type,
        }).done(function(response) {
            var list = response['value'];
            $.each(list, function(i, data) {
                $fullList.push({
                    'type': type,
                    'id': data['id'],
                    'name': data['name'],
                    'maincategory': data['maincategory'],
                    'status': data['status'],
                });
            });

            console.log($fullList);

            $.each($fullList, function(i, data) {
                drawParentList(data, type);
            });



            if ($fullList[0]['status'] == "notavailable") {
                $('#addNewLevel').hide();
            } else {
                $('#addNewLevel').show();
            }

        });
    }

    function checkStatusChild() {
        var x = document.getElementById("parentCategory").value;

        $.ajax({
            url: 'https://codeviable.com/w2w2/public/category/list?maincategory=' + $adsCategory + '&parentid=' + x,
        }).done(function(response) {
            console.log(x);
            var list = response['value'];
            console.log(list);



            if (list.length == 0 || x == "0") {
                $('#addNewLevel').hide();
            } else {
                $('#addNewLevel').show();
            }

        });
    }

    function subListingCategory() {
        var category = $("#parentCategory").val();
        console.log("category")
        console.log(category)
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/category/list?maincategory=' + $adsCategory + '&parentid=' + category,
        }).done(function(response) {
            $fullList = [];
            var list = response['value'];
            $.each(list, function(i, data) {
                $fullList.push({
                    'id': data['id'],
                    'name': data['name'],
                    'maincategory': data['maincategory'],
                    'status': data['status'],
                });
            });

            $("#parentCategory option").remove();
            console.log($fullList);
            $.each($fullList, function(i, data) {
                var div = '';
                div += '<option value=' + data['id'] + '>' + data['name'] + '</option>';
                $('#parentCategory').append(div);
            });



            console.log($fullList[0]);

            if ($fullList[0]['status'] == "notavailable") {
                $('#addNewLevel').hide();
            } else {
                $('#addNewLevel').show();
            }
        });
    }

    function ResetCategory() {
        var category = $("#parentCategory").val();
        console.log("category")
        console.log(category)
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/category/list?maincategory=' + $adsCategory,
        }).done(function(response) {
            $fullList = [];
            var list = response['value'];
            $.each(list, function(i, data) {
                $fullList.push({
                    'id': data['id'],
                    'name': data['name'],
                    'maincategory': data['maincategory'],
                    'status': data['status'],
                });
            });

            $("#parentCategory option").remove();
            console.log($fullList);
            $.each($fullList, function(i, data) {
                var div = '';
                div += '<option value=' + data['id'] + '>' + data['name'] + '</option>';
                $('#parentCategory').append(div);
            });



            console.log($fullList[0]);

            if ($fullList[0]['status'] == "notavailable") {
                $('#addNewLevel').hide();
            } else {
                $('#addNewLevel').show();
            }
        });
    }

    function toggleSideMenu() {
        $('.sideMenu').toggleClass('hide');
        $('.sideMenu').children('div').scrollTop('0');
        $('.myaccount-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        $('.myinventory-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        $('.makeanoffer-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        $('.myaccount-child').addClass('hide');
        $('.myinventory-child').addClass('hide');
        $('.makeanoffer-child').addClass('hide');

        if ($('html, body').hasClass('overflow-hidden')) {
            $('html, body').removeClass('overflow-hidden');
        } else {
            $('html, body').addClass('overflow-hidden');
        }
    }
</script>
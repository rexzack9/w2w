<!DOCTYPE html>
<html lang="zxx">
<style>
    @media(max-width:767px) {
        .webView {
            display: none;
        }

        .card-others {
            padding: 16px !important;
        }
    }

    @media(min-width:768px) {
        .mobileNav {
            display: none;
        }
    }

    .fixed-top-custom {
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 1030;
    }

    html {
        scroll-behavior: smooth;
    }

    .nav-item .nav-menu li a {
        color: black !important;
    }

    .nav-style {
        display: block;
        color: black;
        text-align: center;
        /* padding: 2px 15px; */
        padding-bottom: 12px;
        text-decoration: none;
        font-size: 14px;
        font-weight: normal;
    }

    .active-nav {
        border-bottom: 6px solid #27AE60;
        color: #27AE60;
    }

    .header-margin {
        margin-top: auto;
        margin-bottom: auto;
    }

    .btn-oren {
        color: white !important;
        background-color: #FF5700 !important;
        width: 80% !important;
        /* border-radius: 10% !important; */
    }

    .detail-spacing {
        margin-bottom: 15px;
        color: #707070;
    }

    .checked {
        color: orange;
    }

    html * {
        font-family: 'Roboto' !important;
    }

    i {
        font-family: "FontAwesome" !important;

        display: inline-block;
        font-style: normal;
        font-variant: normal;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        width: max-content;
        right: 0;
        /*z-index: 1;*/
    }

    .dropdown-content a {
        color: black;
        padding: 6px;
        text-align: left;
        text-decoration: none;
        display: block;
    }

    .dropdown-list:hover {
        background-color: #D9FCFF;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        padding: 12px 16px;
        z-index: 1;
        font-size: small;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown-content a:hover {
        color: #27AE60;
    }

    .pointer {
        cursor: pointer;
    }

    .hide {
        display: none;
    }

    .badge-orange {
        background-color: #FF7600 !important;
        border: 1px solid #FF7600 !important;
        color: white !important;
    }

    .hide {
        display: none;
    }

    .white-14 {
        color: white;
        font-size: 14px;
    }

    .black-bold {
        color: black;
        font-weight: bold;
        font-size: 14px;
    }

    .gray-bold {
        color: lightgray;
        font-weight: bold;
    }

    .overflow-hidden {
        overflow: hidden;
    }
</style>

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Waste 2 Wealth</title>
    <link rel="icon" type="image/png" sizes="16x16" href="img/website-logo.png">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body style="position: relative; height: auto; min-height: 100vh;">
    <!-- Page Preloder -->
    <!-- <div id="preloder">
        <div class="loader"></div>
    </div> -->

    <div class="mobileNav fixed-top shadow-1" style="background-color: white">

        <!--Navbar-->
        <nav class="navbar navbar-light light-blue lighten-4">
            <!-- Navbar brand -->
            <button onclick="toggleSideMenu()" class="navbar-toggler toggler-example mr-2" type="button" style="height: 38px;"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button>
            <div class="logo" style="margin-right: 56px;">
                <a href="./">
                    <img src="img/website-logo.png" style="width: 76px;">
                </a>
            </div>
            <div class="row">
                <div class="user hide">
                    <div class="dropdown my-auto mr-3">
                        <!-- <button class="btn" style="display: flex; color: white; font-size: 14px; padding: 0"><i style="font-size: 40px; margin-left: 5px; color: gray; background-color: white; border-radius: 50%;" class="fa fa-user-circle"></i></button> -->

                        <!-- <img id="previewProfile" src="./img/avatar.svg" style="height: 38px; width: 38px; border-radius: 50%; object-fit: cover;"> -->

                        <div class="dropdown-content" style="border-radius: 4px; background-color: white; padding: 0">
                            <div style="padding: 10px;">
                                <div onclick="window.location.href='./my-ads.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                    My Ads
                                </div>
                                <div onclick="window.location.href='./profile.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                    My Account
                                </div>
                                <div onclick="window.location.href='./token-list.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                    Buy Token
                                </div>
                            </div>
                            <div style="background-color: #F2F2F2;">
                                <div class="d-flex" style="padding: 10px 10px 10px 20px;">
                                    <div class="my-auto" style="color: #049D19; font-size: 14px;">
                                        <span id="userBalanceToken">0</span> Tokens
                                    </div>
                                    <div class="ml-5">
                                        <button onclick="window.location.href='./logout.php'" class="btn" style="background-color: white; border: 1px solid #CBCBCB; color: #5482FF;">Logout</button>
                                    </div>
                                </div>
                                <!-- <div onclick="window.location.href='./logout.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
    Logout
</div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <button onclick="toggleSideMenu()" class="navbar-toggler toggler-example mr-2" type="button" style="height: 38px;"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button> -->
                <!-- <button class="navbar-toggler toggler-example mr-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button> -->
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                <ul class="navbar-nav">
                    <li class="nav-item justify-content-center">
                        <div class="d-flex my-auto">

                            <div class="guest my-auto">

                                <a href="./signin.html"><button class="btn nav-title">Sign
                                        In</button></a>
                                <span style="font-size: 14px; color: darkgoldenrod">or</span>
                                <div class="dropdown">
                                    <button class="btn nav-title" style="display: flex;">Register
                                        <i style="font-size: 10px; margin-top: 4px; margin-left: 5px" class="fa fa-chevron-down"></i></button>
                                    <div class="dropdown-content" style="border-radius: 4px; padding: 10px; background-color: white">
                                        <div onclick="window.location.href='register-individu.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Individual
                                        </div>
                                        <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                        </div>
                                        <div onclick="window.location.href='register-company.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Company
                                        </div>
                                        <!-- <a href="#">English</a>
    <a href="#">Malay</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="./setup-ads.html"><button class="btn btn-primary mr-1" style="font-size: 12px; font-weight: bold;">Create
                                Ads</button></a>
                    </li>

                    <li class="nav-item justify-content-center">

                    </li>
                    <li class="nav-item  justify-content-center">
                        <a class="nav-link" onclick="window.location.href='./supply.php'" href="#">Available Product <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active justify-content-center">
                        <a class="nav-link" onclick="window.location.href='./demand.php'" href="#">Wanted Product</a>
                    </li>
                    <li class="nav-item justify-content-center">
                        <a class="nav-link contactUs" href="#"> Contact Us</a>
                    </li>
                </ul>
                <!-- Links -->

            </div>
            <!-- Collapsible content -->

        </nav>
        <!--/.Navbar-->
        <div class="sideMenu hide d-flex" style="position: fixed; z-index: 100; width: 100%;">
            <div style="height: 100vh; width: 230px; min-width: 230px; overflow-y: auto; background-color: white; box-shadow: 0px 4px 8px 0px;">
                <!-- head -->
                <div style="width: 100%; background-color: #3A595D; padding: 12px;">
                    <div class="float-right">
                        <i onclick="toggleSideMenu()" class="fa fa-times" style="font-size: 20px; color: white; -webkit-text-stroke: 2px; -webkit-text-stroke-color: #3A595D; opacity: 0.6;"></i>
                    </div>
                    <div style="height: 24px; width: 100%;"></div>
                    <div class="user hide">
                        <div>
                            <img id="previewProfile1" src="./img/avatar.svg" style="height: 50px; width: 50px; border-radius: 50%; object-fit: cover;">
                        </div>
                        <div class="mt-2 white-14 d-flex justify-content-between">
                            <span class="navUserName"></span><span class="white-14" style="font-size: 14px;"><span class="navUserToken"></span> Token(s)</span>
                        </div>
                        <div class="navUserType gray-bold" style="font-size: 12px;"></div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                    </div>
                    <div class="guest">
                        <div onclick="window.location.href=`./signin.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                            Sign In
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div class="white-14 d-flex justify-content-between myaccount" style="padding-top: 5px; padding-bottom: 5px;">
                            Register <span><i class="myaccount-icon fa fa-chevron-down"></i></span>
                        </div>
                        <div class="myaccount-child hide" style="font-size: 14px; color: white;">
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./register-individu.html`" class="mb-1 mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                As Individual
                            </div>
                            <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./register-company.html`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                As Company
                            </div>
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                    </div>
                    <div onclick="window.location.href=`./setup-ads.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                        Create Ads <span><i class="fa fa-plus-circle"></i></span>
                    </div>
                    <div class="user hide">
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div onclick="window.location.href=`./my-ads.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                            My Ads
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div class="white-14 d-flex justify-content-between myaccount" style="padding-top: 5px; padding-bottom: 5px;">
                            My Account <span><i class="myaccount-icon fa fa-chevron-down"></i></span>
                        </div>
                        <div class="myaccount-child hide" style="font-size: 14px; color: white;">
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./profile.html`" class="mb-1 mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                Profile
                            </div>
                            <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./wallet.html`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                My Wallet
                            </div>
                            <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                            <div onclick="window.location.href=`./logout.php`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                Logout
                            </div>
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                        </div>
                        <div onclick="window.location.href=`./token-list.php`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                            Buy Token
                        </div>
                    </div>
                </div>
                <div style="padding: 12px;">
                    <div onclick="window.location.href=`./supply.php`" class="d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                        Available Product
                    </div>
                    <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.3;"></div>
                    <div onclick="window.location.href=`./demand.php`" class="d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                        Wanted Product
                    </div>
                    <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.3;"></div>
                    <div class="d-flex justify-content-between contactUs" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                        Contact Us
                    </div>
                </div>
            </div>
            <div onclick="toggleSideMenu()" style="background-color: black; height: 100vh; width: 100%; opacity: 0.5;">

            </div>
        </div>
    </div>


    <div class="webView fixed-top">
        <div class="d-flex flex-fill">
            <div class="shadow-1" style="background-color: white; width: 100%; padding-top: 5px; padding-bottom: 5px;">
                <div class="col-md-10 offset-md-1 " style="padding-top: 5px; padding-bottom: 5px;">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex justify-content-between">
                            <div class="logo" style="width: 76px;">
                                <a href="./">
                                    <img src="img/website-logo.png">
                                </a>
                            </div>
                            <div class="d-flex ml-3">
                                <div onclick="window.location.href='./supply.php'" class="nav-title my-auto ml-3 pointer">
                                    Available Product
                                </div>
                                <div onclick="window.location.href='./demand.php'" class="nav-title my-auto ml-3 pointer">
                                    Wanted Product
                                </div>
                                <div class="nav-title my-auto ml-3 pointer contactUs">
                                    Contact Us
                                </div>
                            </div>
                        </div>
                        <div class="d-flex my-auto">
                            <div class="guest my-auto">
                                <a href="./signin.html"><button class="btn nav-title">Sign
                                        In</button></a>
                                <span style="font-size: 14px; color: darkgoldenrod">or</span>
                                <div class="dropdown">
                                    <button class="btn nav-title" style="display: flex;">Register
                                        <i style="font-size: 10px; margin-top: 4px; margin-left: 5px" class="fa fa-chevron-down"></i></button>
                                    <div class="dropdown-content" style="border-radius: 4px; padding: 10px; background-color: white">
                                        <div onclick="window.location.href='register-individu.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Individual
                                        </div>
                                        <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                        </div>
                                        <div onclick="window.location.href='register-company.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                            Register as Company
                                        </div>
                                        <!-- <a href="#">English</a>
                                    <a href="#">Malay</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="user hide">
                                <div class="dropdown my-auto mr-3">
                                    <!-- <button class="btn" style="display: flex; color: white; font-size: 14px; padding: 0"><i style="font-size: 40px; margin-left: 5px; color: gray; background-color: white; border-radius: 50%;" class="fa fa-user-circle"></i></button> -->
                                    <div>
                                        <img id="previewProfile" src="./img/avatar.svg" style="height: 42px; width: 42px; border-radius: 50%; object-fit: cover;">
                                    </div>
                                    <div class="dropdown-content" style="border-radius: 4px; background-color: white; padding: 0">
                                        <div style="padding: 10px;">
                                            <div onclick="window.location.href='./my-ads.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                My Ads
                                            </div>
                                            <div onclick="window.location.href='./profile.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                My Account
                                            </div>
                                            <div onclick="window.location.href='./token-list.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Buy Token
                                            </div>
                                            <!-- <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                            </div> -->
                                        </div>
                                        <div style="background-color: #F2F2F2;">
                                            <div class="d-flex" style="padding: 10px 10px 10px 20px;">
                                                <div class="my-auto" style="color: #049D19; font-size: 14px;">
                                                    <span id="userBalanceToken1">0</span> Tokens
                                                </div>
                                                <div class="ml-5">
                                                    <button onclick="window.location.href='./logout.php'" class="btn" style="background-color: white; border: 1px solid #CBCBCB; color: #5482FF;">Logout</button>
                                                </div>
                                            </div>
                                            <!-- <div onclick="window.location.href='./logout.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Logout
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <a href="./login.html"><button class="btn"
                                style="color: white; font-size: 14px;">Register</button></a> -->
                            <a href="./setup-ads.html"><button class="btn btn-primary" style="font-size: 14px; font-weight: bold;padding:10px">Create
                                    Ads</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- <div class="col-md-8 row" style="padding-bottom: 30px;">
        <a href="./index.html" style="text-decoration: underline; color: black;">Home</a>
        <div>&nbsp;>>&nbsp;</div>
        <a href="./productlistings-viewall.html" style="text-decoration: underline; color: black;">Available</a>
        <div>&nbsp;>>&nbsp;</div>
        <a style="text-decoration: underline;">Entire Malaysia</a>
        <div>&nbsp;>>&nbsp;</div>
        <a id="maincategory" style="text-decoration: underline;">Waste</a>
        <div>&nbsp;>>&nbsp;</div>
        <a id="name" style="font-weight: bold; color: #008D50;"></a>
    </div> -->

    <div style="margin-bottom: 30px; padding-top: 100px;">
        <div class="row" style="margin: 0;">
            <div class="col-12 col-md-10 offset-md-1 col-sm-8 offset-sm-2">
                <div class="row" style="margin: 0;">
                    <div class="col-md-4" style="padding: 0;">
                        <img id="main_image" src="../W2W/admin/plugins/images/bg.jpg" style="width: 398px; height: 372px; object-fit: cover; ">
                        <!-- width: 398px; height: 372px; -->
                        <div id="sub_image" class="row" style="margin-top: 20px;">

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="id"></div>
                        <div id="product_name" style="color: gray;"></div>
                        <div id="availability" class="badge"></div>
                        <!-- <label id="availability" style="color: white; background-color: green; border-radius: 10px; padding: 5px;">Available</label> -->
                        <div style="margin-top: 55px;">
                            <div id="product_price" style="font-size: 26px; font-weight: bold;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="background-color: #F2F2F2; padding-bottom: 94px;">
        <div class="row" style="margin: 0;">
            <div class="col-12 col-md-10 offset-md-1" style="margin-top: 46px;">
                <div class="card" style="box-shadow: 0px 0px 4px #00000029;">
                    <div style="background-color: #F2F2F2; padding: 10px; margin: 10px;">Material Information</div>
                    <div style="padding: 20px;">
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Date of submission</label>
                            </div>
                            <div class="col-md-8">
                                <div id="date" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Material Status</label>
                            </div>
                            <div class="col-md-8">
                                <div id="mainstatus" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label class="isWaste hide">Name of Material</label>
                                <label class="isTechnology hide">Name of Technology</label>
                                <label class="isService hide">Name of Service</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_name2" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing isWaste hide">
                            <div class="col-md-4">
                                <label>State of Material</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_material" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Category</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_category" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing isWaste hide">
                            <div class="col-md-4">
                                <label>Product Season</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_continuity" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing isWaste hide">
                            <div class="col-md-4">
                                <label>Transportation Mode</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_transportation" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Additional Information</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_desc" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing isWaste hide">
                            <div class="col-md-4">
                                <label>Quantity</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_quantityunit" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing isWaste hide">
                            <div class="col-md-4">
                                <label>Shelf Life/ Degradation period</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_period" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Suggested Supplier/Buyer</label>
                            </div>
                            <div class="col-md-8">
                                <div id="suggestedcustomer" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Selling Price</label>
                            </div>
                            <div class="col-md-8">
                                <div id="product_price2" style="color: black;">-</div>
                            </div>
                        </div>

                        <div id="location_display">
                            <!-- <div class="row detail-spacing">
                    <div class="col-md-4">
                        <label>Waste (location 1)</label>
                    </div>
                    <div class="col-md-8">
                        <div id="product_location1">Pasir Gudang</div>
                    </div>
                </div>
                <div class="row detail-spacing">
                    <div class="col-md-4">
                        <label>State</label>
                    </div>
                    <div class="col-md-8">
                        <div id="product_state1">Johor</div>
                    </div>
                </div>
                <div class="row detail-spacing">
                    <div class="col-md-4">
                        <label>Waste (location 2)</label>
                    </div>
                    <div class="col-md-8">
                        <div id="product_location2">Kulai</div>
                    </div>
                </div>
                <div class="row detail-spacing">
                    <div class="col-md-4">
                        <label>State</label>
                    </div>
                    <div class="col-md-8">
                        <div id="product_state2">Johor</div>
                    </div>
                </div> -->
                        </div>
                        <div id="googleMap" style="height:400px;"></div>
                        <br>
                        <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Requester</label>
                            </div>
                            <div class="col-md-8">
                                <div id="supplier" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing isTechnology hide">
                            <div class="col-md-4">
                                <label>Target Industry</label>
                            </div>
                            <div class="col-md-8">
                                <div id="targetIndustry" style="color: black;">-</div>
                            </div>
                        </div>
                        <div class="row detail-spacing isTechnology isService hide">
                            <div class="col-md-4">
                                <label>Website</label>
                            </div>
                            <div class="col-md-8">
                                <div id="website" style="color: black;">-</div>
                            </div>
                        </div>
                        <!-- <div class="row detail-spacing">
                            <div class="col-md-4">
                                <label>Company Name</label>
                            </div>
                            <div class="col-md-8">
                                <div id="companyname">-</div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

        <div id="question" class="row" style="margin: 0;">
            <div class="col-12 col-md-10 offset-md-1" style="margin-top: 28px; margin-bottom: 28px;">
                <div class="card card-others" style="padding: 30px; box-shadow: 0px 0px 4px #00000029;">
                    <div style="font-size: 24px; font-weight: bold;">Question about this product</div>
                    <div style="text-align: right;"></div>
                    <div style="border-bottom: 2px solid lightgray;"></div>
                    <div id="listcomment" style="padding: 20px;">
                        <div id="beforeask" style="padding: 20px;">
                            <div class="text-center">There are no question yet.</div>
                            <div class="text-center">Ask the seller now and their answer will be hear.</div>
                            <div id="ask_btn" class="text-center" style="color: #FF7600; margin-top: 10px; font-weight: bold; cursor: pointer;">Ask Question</div>
                        </div>


                    </div>
                    <!-- <form id="askform" onsubmit="return false"> -->
                    <div id="askquestion" style="margin-top: 30px; display: none;">
                        <textarea id="askquestion2" cols="128" rows="2" placeholder="Ask your question..." class="form-control" required></textarea>
                        <div id="error" style="color: red; font-size: 14px;"></div>
                        <br>
                        <div id="submit" type="button" style="cursor: pointer; color: #FF7600; margin-top: 10px; font-weight: bold; text-align: right;">Submit</div>
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
        <!-- <div class="row" style="margin: 0;">
            <div class="col-10 offset-1" style="margin-top: 26px; margin-bottom: 100px;">
                <div style="font-size: 24px; font-weight: 500;">Similar Ads</div>
                <div class="row" style="margin-top: 24px;">
                    <div class="col-md-3">
                        <div class="d-flex flex-column justify-content-between" style="height: 300px; padding: 20px; background-color: white; border-radius: 6px; box-shadow: 0px 0px 2px #00000029;">
                            <div style="display: flex; height: 100%;">
                                <img src="img/petroleum.jpg" style="margin-top: auto; margin-bottom: auto; height: -webkit-fill-available; object-fit: cover;">
                            </div>
                            <div style="color: #8B8B8B; font-size: 14px; margin-top: 18px;">
                                Organic Compose
                            </div>
                            <div style="font-size: 24px;">
                                RM 100
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="d-flex flex-column justify-content-between" style="height: 300px; padding: 20px; background-color: white; border-radius: 6px; box-shadow: 0px 0px 2px #00000029;">
                            <div style="display: flex; height: 100%;">
                                <img src="img/energy.jpg" style="margin-top: auto; margin-bottom: auto; height: -webkit-fill-available; object-fit: cover;">
                            </div>
                            <div style="color: #8B8B8B; font-size: 14px; margin-top: 18px;">
                                Organic Compose
                            </div>
                            <div style="font-size: 24px;">
                                RM 100
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="d-flex flex-column justify-content-between" style="height: 300px; padding: 20px; background-color: white; border-radius: 6px; box-shadow: 0px 0px 2px #00000029;">
                            <div style="display: flex; height: 100%;">
                                <img src="img/petroleum.jpg" style="margin-top: auto; margin-bottom: auto; height: -webkit-fill-available; object-fit: cover;">
                            </div>
                            <div style="color: #8B8B8B; font-size: 14px; margin-top: 18px;">
                                Organic Compose
                            </div>
                            <div style="font-size: 24px;">
                                RM 100
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="d-flex flex-column justify-content-between" style="height: 300px; padding: 20px; background-color: white; border-radius: 6px; box-shadow: 0px 0px 2px #00000029;">
                            <div style="display: flex; height: 100%;">
                                <img src="img/energy.jpg" style="margin-top: auto; margin-bottom: auto; height: -webkit-fill-available; object-fit: cover;">
                            </div>
                            <div style="color: #8B8B8B; font-size: 14px; margin-top: 18px;">
                                Organic Compose
                            </div>
                            <div style="font-size: 24px;">
                                RM 100
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>


    <!-- Footer begin -->
    <footer id="footer" class="footer-section" style="background-color: #4B4B4B; position: absolute; bottom: 0; width: 100%">
        <div class="container">
            <div class="row" style="padding-top: 5px;">
                <div class="col-lg-2">
                    <a onclick="$(`#contactUsModal`).modal()" style="color: white; cursor: pointer;">Contact Us</a>
                </div>
                <div class="col-lg-4">
                    <a href="./assets/tnc/Terms and Conditions of W2W.pdf" target="_blank" style="color: white;">Terms and Conditions</a>
                </div>
                <div class="col-lg-5">
                    <p style="color: white;">
                        Copyright &copy; Waste 2 Wealth Website. All rights reserved
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- Sign in modal -->
    <div class="modal fade" id="modalSignIn">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="login-form">
                        <h2 style="text-align: left; margin-bottom: 10px;">Sign in</h2>
                        <div class="login-text" style="text-align: left; padding-bottom: 20px">
                            <p>New User?
                                <a href="./select-register.html" class="or-login" style="color: #0099FF;">Create an account</a>
                            </p>
                        </div>
                        <div class="form-group">
                            <label class="login-text" for="username">Email </label>
                            <input type="text" class="form-control" id="email" required>
                        </div>
                        <div class="form-group">
                            <label class="login-text" for="pass">Password </label>
                            <input type="password" class="form-control" id="password" required>
                        </div>
                        <!-- <div class="group-input">
                                    <div class="gi-more">
                                        <label class="login-text" for="save-pass">
                                            Keep me signed in.
                                            <input type="checkbox" id="save-pass">
                                            <span id="keepme_signedin" class="checkmark"></span>
                                        </label>
                                    </div>
                                </div> -->
                        <div>
                            <button id="signin" value="submit" class="btn btn-block" style="height: 56px; background-color: #3A595D; border: 1px solid #3A595D; color: white;">Sign In</button>
                        </div>
                        <div>
                            <!-- <a href="#" class="forget-pass float-right login-text" style="color: #D91F1F; padding-top: 20px;">Forgot pasword?</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- reserve modal -->
    <div class="modal fade" id="modalReserve" role="dialog" style="top: 10%; left: 5%;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="padding: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div style="font-size: 28px; font-weight: bold; margin-bottom: 30px;">Make an offer</div>
                    <form id="reserveform" onsubmit="return false">
                        <div class="row text-padding" style="margin-bottom: 20px;">

                            <div class="col-md-3" style="margin-top: auto; margin-bottom: auto;">
                                <div>Price</div>
                            </div>
                            <div class="col-md-2" style="margin-top: auto; margin-bottom: auto;">
                                <div>RM</div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0;">
                                <input id="offerprice" type="text" class="form-control" pattern="[0-9]+([,\.][0-9]+)?" required>
                            </div>
                        </div>
                        <div class="row text-padding" style="margin-bottom: 20px;">
                            <div class="col-md-3" style="margin-top: auto; margin-bottom: auto;">
                                <div>Quantity</div>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;">
                                <input id="quantity_reserve" type="text" class="form-control" pattern="\d*" required>
                            </div>
                            <div class="col-md-2 text-right" style="margin-top: auto; margin-bottom: auto;">
                                <div>Unit</div>
                            </div>
                            <div class="col-md-3" style="padding-left: 0;">
                                <input id="unit_reserve" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="row text-padding" style="margin-bottom: 20px;">
                            <div class="col-md-12" style="margin-top: auto; margin-bottom: auto;">
                                <textarea id="additional_reserve" type="text" class="form-control" placeholder="Additional Information"></textarea>
                            </div>
                        </div>
                        <button id="makeoffer_btn" class="btn btn-oren" style="width: 100% !important; margin-bottom: 10px;">Make an offer</button>
                        <button class="btn btn-oren-cancell" style="width: 100% !important;" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- contact us Modal -->
    <div class="modal fade" id="contactUsModal" tabindex="-1" role="dialog" aria-labelledby="contactUsModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div style="text-align: center; font-size: 30px; margin-top: 10px;">Contact Us</div>
                    <div style="padding: 60px;">
                        <div id="successMsg" class="hide">
                            <div style="font-size: 14px; color: #269635;">
                                Thank you for your message. It has been sent!
                            </div>
                            <br>
                            <div>
                                <button onclick="$('#contactUsModal').modal('hide')" class="btn btn-block" style="background-color: #27AE60; border: 1px solid #27AE60; color: white;">Okay</button>
                            </div>
                        </div>
                        <div id="defaultContact">
                            <div>
                                <input id="contactUsName" type="text" class="form-control" placeholder="Name…">
                            </div>
                            <br>
                            <div>
                                <input id="contactUsEmail" type="email" class="form-control" placeholder="someone@example.com">
                            </div>
                            <br>
                            <div>
                                <select name="contactUsRegarding" id="contactUsRegarding" class="form-control">
                                    <option value="nothing">Regarding...</option>
                                    <option value="support">Support</option>
                                    <option value="report">Report</option>
                                </select>
                            </div>
                            <br>
                            <div>
                                <textarea id="contactUsMessage" class="form-control" cols="20" rows="7"></textarea>
                            </div>
                            <br>
                            <div>
                                <button id="sendContactUs" class="btn btn-block" style="background-color: #27AE60; border: 1px solid #27AE60; color: white;">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.zoom.min.js"></script>
    <script src="js/jquery.dd.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>

<script>
    $(window).resize(function() {
        console.log('resize called');
        var width = $(window).width();
        var height = $(window).height();
        var newHeight = height - 60;

        $('.sideMenu').children('div').css('height', newHeight);

        if (width <= 768) {
            // $('.sideMenu').removeClass('hide');
        } else {
            $('.sideMenu').addClass('hide');
        }
    }).resize()

    $(document).ready(function() {
        $product_id = localStorage.getItem('w2wdemand_detail');
        $user = null;
        $mainImage = '';

        getUser();
        productDetails();

        // myMap();
        displaycomment();
        check_val();
        listRating();

        $('#listcomment').delegate('#ask_btn', 'click', function() {
            $('#beforeask').css('display', 'none');
            $('#askquestion').css('display', 'block');
        });

        $('#submit').on('click', function() {
            console.log('submit');
            if ($user != null) {
                $('#error').html('');
                if ($('#askquestion2').val() == "") {
                    $('#error').html('Please fill this field');
                } else {
                    $('#submit').attr('disabled', true);
                    postComment();
                }
            } else {
                $('#modalSignIn').modal();
            }
        });

        //signin
        // var highlightForm = document.querySelector("form#signinform");
        // highlightForm.addEventListener('submit', highlight, false);

        // function highlight(event) {
        //     event.preventDefault();
        //     return false;

        // }

        $('#reserve_btn').on('click', function() {
            if ($user != null) {
                $('#modalReserve').modal();
            } else {
                $('#modalSignIn').modal();
            }
        });

        $('#signin').on('click', function() {
            var email = $('#email').val();
            var password = $('#password').val();

            $postData = {
                'user_email': email,
                'password': password,
            };

            login($postData);
        });

        $('.contactUs').on('click', function() {
            drawContactUs($user);
            $('#contactUsModal').modal();
        });

        $('#sendContactUs').on('click', function() {
            var name = '';
            var email = '';
            var category = '';
            var message = '';
            name = $('#contactUsName').val();
            email = $('#contactUsEmail').val();
            category = $('#contactUsRegarding').val();
            message = $('#contactUsMessage').val();

            if (name != '' && email != '' && category != 'nothing' && message != '') {
                $postData = {
                    'name': name,
                    'email': email,
                    'category': category,
                    'description': message,
                };

                postContactUs($postData);
            }
        });

        $("#contactUsModal").on("hidden.bs.modal", function() {
            $('#contactUsName').val('');
            $('#contactUsEmail').val('');
            $('#contactUsRegarding').val('nothing');
            $('#contactUsMessage').val('');
            $('#successMsg').addClass('hide');
            $('#defaultContact').removeClass('hide');
            $('#sendContactUs').attr('disabled', false);
        });

        $('#sub_image').delegate('.otherImage', 'mouseenter mouseleave', function() {
            var image = $(this).attr('src');
            if (event.type == 'mouseover') {
                $('#main_image').attr('src', image);
            } else if (event.type == 'mouseout') {
                $('#main_image').attr('src', $mainImage);
            }
        });

        $('.myaccount').on('click', function() {
            $('.myaccount-child').toggleClass('hide');

            if ($('.myaccount-icon').hasClass('fa-chevron-down')) {
                $('.myaccount-icon').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            } else {
                $('.myaccount-icon').addClass('fa-chevron-down').removeClass('fa-chevron-up');
            }
        });
    });

    function postComment() {
        var question = $('#askquestion2').val();

        $.ajax({
            url: "https://codeviable.com/w2w2/public/comment",
            method: "POST",
            data: {
                'product_id': $product_id,
                'description': question,
                'user_id': $user['user_id'],
            },
        }).done(function(response) {
            console.log(response);
            $('#questionmodal').modal('toggle');
            $("#questionmodal").on('hide.bs.modal', function() {
                location.reload();
            });
            goToQuestion();
            displaycomment();
        });
    }

    function goToQuestion() {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#question").offset().top
        }, 500);
    }

    function submitcomment() {
        console.log('submit');
        if ($user != null) {
            $('#error').html('');
            if ($('#askquestion2').val() == "") {
                $('#error').html('Please fill this field');
            } else {
                $('#submit').attr('disabled', true);
                postComment();
            }
        } else {
            $('#modalSignIn').modal();
        }
    }

    function postContactUs(postData) {
        $('#sendContactUs').attr('disabled', true);
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/enquiry',
            method: 'POST',
            data: postData,
        }).done(function(response) {
            $('#successMsg').removeClass('hide');
            $('#defaultContact').addClass('hide');
            console.log(response);
        });
    }

    function check_val() {
        var bad_words = new Array("shit", "f*ck", "fuck", "piss off", "dick head", "asshole", "son of bitch", "son of b*tch",
            "bastard", "bitch", "damn", "bloody hell", "rubbish", "bodoh", "gila", "babi", "teruk");
        var check_text = document.getElementById("askquestion2").value;
        var error = 0;
        for (var i = 0; i < bad_words.length; i++) {
            var val = bad_words[i];
            if ((check_text.toLowerCase()).indexOf(val.toString()) > -1) {
                error = error + 1;
            }
        }

        if (error > 0) {
            document.getElementById("submit").style.visibility = "hidden";
            document.getElementById("error").innerHTML = "Some bad words in your comment.";
        } else {
            document.getElementById("submit").style.visibility = "visible";
            document.getElementById("error").innerHTML = "";
        }
    }

    function getTokenBalance() {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/token/mytoken?userid=' + $user['user_id'],
        }).done(function(response) {
            console.log(response);
            var data = response['value'];
            $('#userBalanceToken').html(data['tokenbalance']);
            $('#userBalanceToken1').html(data['tokenbalance']);
            $('.navUserToken').html(data['tokenbalance']);
        });
    }

    function getUser() {
        $user = JSON.parse(localStorage.getItem('w2wUser'));
        if ($user != null) {
            drawContactUs($user);
            var profilePicture = $user['profilepicture'];
            var extension = profilePicture.substr(profilePicture.length - 4);
            // if (extension.indexOf('png') >= 1 || extension.indexOf('jpg') >= 1 || extension.indexOf('jpeg') >= 1) {
            $('#previewProfile').attr('src', profilePicture);
            $('#previewProfile1').attr('src', profilePicture);

            if ($user['user_type'] != 'company') {
                $('.navUserName').html($user['user_fname']).css('text-transform', 'capitalize');
            } else {
                $('.navUserName').html($user['companyname']).css('text-transform', 'capitalize');
            }
            $('.navUserType').html($user['user_type']).css('text-transform', 'capitalize');
            // }
            if ($user['balancetoken'] != null) {
                $('#userBalanceToken').html($user['balancetoken']);
                $('#userBalanceToken1').html($user['balancetoken']);
                $('.navUserToken').html($user['balancetoken']);
                getTokenBalance();
            } else {
                $('#userBalanceToken').html('0');
                $('#userBalanceToken1').html('0');
                $('.navUserToken').html('0');
            }
            $('.guest').addClass('hide');
            $('.user').removeClass('hide');
        }
    }

    function drawContactUs(data) {
        if (data != '' && data != null) {
            if (data['user_type'] == 'company') {
                $('#contactUsName').val(data['companyname']);
            } else {
                $('#contactUsName').val(data['user_fname'] + ' ' + data['user_lname']);
            }
            $('#contactUsEmail').val(data['user_email']);
        }
    }

    function productDetails() {
        $.ajax({
            url: "https://codeviable.com/w2w2/public/product/detail?product_id=" + $product_id,
            type: "GET",
        }).done(function(response) {
            console.log(response);
            response[0] = response['value'][0];
            localStorage.setItem('product_user', response[0].user_id);
            var status = response[0].availability;
            if (status == 'fulfilled') {
                $('#availability').html('<span style="font-style: italic; font-size: 24px; font-weight: 400;">FULLFILLED</span>').addClass('badge-danger');
            } else if (status == 'available') {
                $('#availability').html('<span style="font-style: italic; font-size: 24px; font-weight: 400;">WANTED!</span>').addClass('badge-orange');
            }
            // supplier_company(response[0]['user_id']);
            $('#maincategory').html(response[0].maincategory).css('text-transform', 'capitalize');
            $('#name').html(response[0].product_name).css('text-transform', 'capitalize');
            $('#id').html(response[0].product_id).css('text-transform', 'capitalize');
            $('#product_name').html(response[0].product_name).css('text-transform', 'capitalize');
            $('#productPrice').html(response[0].product_price);
            $('#product_price').html('RM ' + response[0].product_price).css('text-transform', 'capitalize');
            $('#quantity_unit').html(response[0].product_quantity + " " + response[0].unit);
            $('#date').html(response[0].product_date).css('text-transform', 'capitalize');
            $('#mainstatus').html(response[0].mainstatus).css('text-transform', 'capitalize');
            $('#product_name2').html(response[0].product_name).css('text-transform', 'capitalize');
            $('#supplier').html(response[0].user_name);

            var maincategory = response[0].maincategory;

            if (maincategory === 'waste') {
                $('#originalPrice').html(response[0]['product_price'] + ' / ' + response[0]['product_quantity'] + ' ' + response[0]['unit']);
                $('#offeredUnit').html(' / ' + response[0]['product_quantity'] + ' ' + response[0]['unit']);
                $('#offeredUnit2').html(response[0]['unit']);
                $('.isWaste').removeClass('hide');
            } else {
                $('.isWaste').addClass('hide');
                $('#originalPrice').html(response[0]['product_price']);
            }

            if (maincategory === 'technology') {
                $('.isTechnology').removeClass('hide');
                $('#targetIndustry').html(response[0]['product_target']);
                if (response[0]['website'] != null && response[0]['website'] != "") {
                    $('#website').html(response[0]['website']);
                } else {
                    $('#website').html('-');
                }
            }

            if (maincategory === 'service') {
                $('.isService').removeClass('hide');
                if (response[0]['website'] != null && response[0]['website'] != "") {
                    $('#website').html(response[0]['website']);
                } else {
                    $('#website').html('-');
                }
            }

            if (maincategory == 'waste') {
                $('#product_price').html('RM ' + response[0].product_price + ' / ' + response[0].product_quantity + ' ' + response[0].unit);
                // $('#quantity_unit').html(response[0].product_quantity + " " + response[0].unit);
            } else {
                $('#product_price').html('RM ' + response[0].product_price);
            }

            if (response[0].product_material != null) {
                $('#product_material').html(response[0].product_material).css('text-transform', 'capitalize');
            } else {
                $('#product_material').html('-');
            }

            if (response[0].product_category != null) {
                $('#product_category').html(response[0].product_category).css('text-transform', 'capitalize');
            } else {
                $('#product_category').html('-').css('text-transform', 'capitalize');
            }

            if (response[0].product_continuity != null) {
                $('#product_continuity').html(response[0].product_continuity).css('text-transform', 'capitalize');
            } else {
                $('#product_continuity').html('-').css('text-transform', 'capitalize');
            }

            if (response[0].product_transport != null) {
                $('#product_transportation').html(response[0].product_transport);
            } else {
                $('#product_transportation').html('-');
            }




            $('#main_image').attr('src', response[0].product_image[0].image);
            $mainImage = response[0].product_image[0].image;


            if (response[0].product_description != null && response[0].product_description != "") {
                $('#product_desc').html(response[0].product_description).css('text-transform', 'capitalize');
            } else {
                $('#product_desc').html('-').css('text-transform', 'capitalize');
            }

            if (response[0].product_quantity != null) {
                $('#product_quantityunit').html(response[0].product_quantity + " " + response[0].unit);
            } else {
                $('#product_quantityunit').html('-');
            }

            if (response[0].shellife != null) {
                $('#product_period').html(response[0].shellife).css('text-transform', 'capitalize');
            } else {
                $('#product_period').html('-').css('text-transform', 'capitalize');
            }

            if (response[0].suggestcustomer != null) {
                $('#suggestedcustomer').html(response[0].suggestcustomer).css('text-transform', 'capitalize');
            } else {
                $('#suggestedcustomer').html('-').css('text-transform', 'capitalize');
            }



            if (response[0].product_price == "0.00") {
                $('#product_price2').html('Free');
            } else {
                $('#product_price2').html('RM ' + response[0].product_price).css('text-transform', 'capitalize');
            }

            //product location
            var tr = "";
            var bil = 1;
            console.log('test');
            console.log(response[0]);
            $.each(response[0].product_location, function(i, data) {
                if (response[0].maincategory == "waste" && response[0].product_location[i].location != "")
                    tr += '<div class="row detail-spacing"><div class="col-md-4"><label>Waste (location)</label></div><div class="col-md-8"><div id="product_location1" style="color: black;">' + response[0].product_location[i].location + '</div></div></div>';

                else if (response[0].maincategory == "technology" && response[0].product_location[i].location != "")
                    tr += '<div class="row detail-spacing"><div class="col-md-4"><label>Technology (location)</label></div><div class="col-md-8"><div id="product_location1" style="color: black;">' + response[0].product_location[i].location + '</div></div></div>';

                else if (response[0].maincategory == "service" && response[0].product_location[i].location != "")
                    tr += '<div class="row detail-spacing"><div class="col-md-4"><label>Service (location)</label></div><div class="col-md-8"><div id="product_location1" style="color: black;">' + response[0].product_location[i].location + '</div></div></div>';

                // if (response[0].product_state[i].state !== "null" && response[0].product_state[i].state !== "undefined")
                //     tr += '<div class="row detail-spacing"><div class="col-md-4"><label>State</label></div> <div class="col-md-8"><div id="product_state1" style="color: black;">' + response[0].product_state[i].state + '</div></div></div>'
                bil++;
            });
            tr += '<div class="row detail-spacing"><div class="col-md-4"><label>State</label></div> <div class="col-md-8"><div id="product_state1" style="text-transform: capitalize; color: black;">' + response[0].product_state[0].state + '</div></div></div>'

            $('#location_display').html(tr);



            var tr2 = "";
            var no = 1;
            $.each(response[0].product_image, function(i, data) {
                if (i != 0) {
                    tr2 += '<div class="col-md-3"><img class="otherImage" src=' + response[0].product_image[i].image + ' style="width: 70px; height: 50px; object-fit: cover;"></div>';
                }
                no++;
            });
            $('#sub_image').html(tr2);

            localStorage.setItem('latitude', response[0].latitud[0].latitud);
            localStorage.setItem('longitude', response[0].longitud[0].longitud);
        });

    }


    function supplier_company(id) {
        var product_user = localStorage.getItem('product_user');

        $.ajax({
            url: "https://codeviable.com/w2w2/public/user/detail?user_id=" + id,
        }).done(function(response) {
            console.log(response)
            $('#supplier').html(response['value']['user_fname'] + " " + response['value']['user_lname']).css('text-transform', 'capitalize');

            if (response.companyname != null) {
                S$('#companyname').html(response.companyname);
            }
        });
    }

    function login(postData) {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/login',
            method: 'POST',
            data: postData,
        }).done(function(response) {
            console.log(response);
            if (response['status'] == 'success') {
                getUserDetails(response['value'][0]['user_id']);
            } else {
                $('#login').attr('disabled', false);
            }
        });
    }

    function getUserDetails(id) {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/user/detail?user_id=' + id,
        }).done(function(response) {
            $('#login').attr('disabled', false);
            if (response['status'] == 'success') {
                localStorage.setItem('w2wUser', JSON.stringify(response['value']));
                document.cookie = 'w2wLogged=true';
                window.location.reload();
            }
        });
    }


    var lat = localStorage.getItem('latitude');
    var lng = localStorage.getItem('longitude');

    function myMap() {
        var mapProp = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 15,
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map
        });
    }
    $(document).ready(myMap);

    function comment() {
        var comment = $('#askquestion2').val();
        var postdata = {
            "product_id": $product_id,
            "description": comment,
            "user_id": $user['user_id'],
        }

        $.ajax({
            url: "https://codeviable.com/w2w2/public/comment?product_id=&title=&description=",
            type: "POST",
            data: postdata
        }).done(function(response) {
            console.log(response);
            $('#questionmodal').modal('toggle');
            $("#questionmodal").on('hide.bs.modal', function() {
                location.reload();
            });
            displaycomment();
        });
    }

    function displaycomment() {
        $.ajax({
            url: "https://codeviable.com/w2w2/public/comment/listing?product_id=",
            type: "GET",
            data: {
                "product_id": $product_id
            }
        }).done(function(response) {
            $('#submit').attr('disabled', false);
            console.log(response);
            var response = response['value'];
            var tr = "";
            if (response.length == 0) {
                tr += '<div id="beforeask" style="padding: 20px;"><div class="text-center">There are no question yet.</div><div class="text-center">Ask the seller now and their answer will be hear.</div><div id="ask_btn" class="text-center ask_btn" style="color: #FF7600; margin-top: 10px; font-weight: bold; cursor: pointer;">Ask Question</div></div>'
                tr += '<form id="askform" onsubmit="return false"><div id="askquestion" style="margin-top: 30px; display: none;"><textarea onKeyUp="check_val()" id="askquestion2" cols="128" rows="2" placeholder="Ask your question..." class="form-control" required></textarea><div id="error" style="margin-top: 10px; color: red; font-size: 14px;"></div><br><button id="submit" class="btn float-right" onClick="submitcomment()" style="cursor: pointer; color: #FF7600; margin-top: 10px; font-weight: bold; text-align: right;">Submit</button></div></form>';
            } else if (response.length != 0) {
                $.each(response, function(i, data) {
                    var timestamp = data.created_at.substring(14, 19);
                    var date = data.created_at[0] + '' + data.created_at[1] + ' ' + data.created_at[3] + data.created_at[4] + data.created_at[5] + ' ' + data.created_at[7] + data.created_at[8] + data.created_at[9] + data.created_at[10];
                    tr += '<div class="row detail-spacing"><div class="col-md-4"><div style="color: #0099FF; margin-top: 5px; font-size: 16px; text-transform: capitalize;">' + data.user_fname + ' ' + data.user_lname + '</div><div style="margin-top: 5px; font-size: 12px;">' + date + '</div><div style="margin-top: 5px; font-size: 10px;">' + timestamp + '</div></div><div class="col-md-8"><div>' + data.description + '</div></div></div>';
                });
                tr += '<form id="askform" onsubmit="return false"><div id="askquestion" style="margin-top: 30px;"><textarea onKeyUp="check_val()" id="askquestion2" cols="128" rows="2" placeholder="Ask your question..." class="form-control" required></textarea><div id="error" style="color: red; font-size: 14px; margin-top: 10px;"></div><br><button id="submit" class="btn float-right" onClick="submitcomment()" style="cursor: pointer; color: #FF7600; margin-top: 10px; font-weight: bold; text-align: right;">Submit</button></div></form>';
            }
            $('#listcomment').html(tr)
        })
    }


    // function displaycomment() {
    //     $.ajax({
    //         url: "https://codeviable.com/w2w2/public/comment/listing?product_id=",
    //         type: "GET",
    //         data: {
    //             "product_id": $product_id
    //         }
    //     }).done(function(response) {
    //         var response = response['value'];
    //         var tr = "";
    //         if (response.length == 0) {
    //             tr += '<div id="beforeask" style="padding: 20px;"><div class="text-center">There are no question yet.</div><div class="text-center">Ask the seller now and their answer will be hear.</div><div id="ask_btn" class="text-center ask_btn" style="color: #FF7600; margin-top: 10px; font-weight: bold; cursor: pointer;">Ask Question</div></div>'
    //             tr += '<form id="askform" onsubmit="return false"><div id="askquestion" style="margin-top: 30px; display: none;"><textarea onKeyUp="check_val()" id="askquestion2" cols="128" rows="2" placeholder="Ask your question..." class="form-control" required></textarea><div id="error" style="margin-top: 10px; color: red; font-size: 14px;"></div><br><div id="submit" type="button" style="cursor: pointer; color: #FF7600; margin-top: 10px; font-weight: bold; text-align: right;">Submit</div></div></form>';
    //         } else if (response.length != 0) {
    //             $.each(response, function(i, data) {
    //                 var timestamp = data.created_at.substring(14, 19);
    //                 var date = data.created_at[0] + '' + data.created_at[1] + ' ' + data.created_at[3] + data.created_at[4] + data.created_at[5] + ' ' + data.created_at[7] + data.created_at[8] + data.created_at[9] + data.created_at[10];
    //                 tr += '<div class="row detail-spacing"><div class="col-md-4"><div style="color: #0099FF; margin-top: 5px; font-size: 16px; text-transform: capitalize;">' + data.user_fname + ' ' + data.user_lname + '</div><div style="margin-top: 5px; font-size: 12px;">' + date + '</div><div style="margin-top: 5px; font-size: 10px;">' + timestamp + '</div></div><div class="col-md-8"><div>' + data.description + '</div></div></div>';
    //             });
    //             tr += '<form id="askform" onsubmit="return false"><div id="askquestion" style="margin-top: 30px;"><textarea onKeyUp="check_val()" id="askquestion2" cols="128" rows="2" placeholder="Ask your question..." class="form-control" required></textarea><div id="error" style="color: red; font-size: 14px; margin-top: 10px;"></div><br><div id="submit" type="button" style="cursor: pointer; color: #FF7600; margin-top: 10px; font-weight: bold; text-align: right;">Submit</div></div></form>';
    //         }
    //         $('#listcomment').html(tr)
    //     })
    // }

    function listRating() {
        $.ajax({
            url: "https://codeviable.com/w2w2/public/review/list?product_id=",
            type: "GET",
            data: {
                "product_id": $product_id
            }
        }).done(function(response) {
            console.log(response);

            var row = "";
            var tr = "";
            $.each(response, function(i, data) {
                // var date = data.created_at[8]+''+data.created_at[9]+'/'+data.created_at[5]+''+data.created_at[6]+'/'+data.created_at[0]+''+data.created_at[1]+''+data.created_at[2]+''+data.created_at[3];

                row += '<div class="row detail-spacing"><div class="col-md-4"><div id="star"></div><div style="color: blue; margin-top: 5px; text-transform: capitalize;">' + response[i].user_fname + " " + response[i].user_lname + '</div><div style="margin-top: 5px; font-size: 12px;"></div></div><div class="col-md-8"><div>' + response[i].description + '</div></div></div>';

                for (var a = 0; a < 5; a++) {
                    if (a < response[i].rating)
                        tr += '<i class="fa fa-star checked"></i>';
                    else
                        tr += '<i class="fa fa-star"></i>';
                }
            });

            $('#rating_list').html(row);
            $('#star').html(tr)
        })
    }

    function toggleSideMenu() {
        $('.sideMenu').toggleClass('hide');
        $('.sideMenu').children('div').scrollTop('0');
        $('.myaccount-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        $('.myinventory-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        $('.makeanoffer-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        $('.myaccount-child').addClass('hide');
        $('.myinventory-child').addClass('hide');
        $('.makeanoffer-child').addClass('hide');

        if ($('html, body').hasClass('overflow-hidden')) {
            $('html, body').removeClass('overflow-hidden');
        } else {
            $('html, body').addClass('overflow-hidden');
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoXDv2cA1qRqsUdsEwpRb9OLa3rSN1qrs&callback=myMap"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCFV9i4F6pI4UF94WD-7k-TZ0ZzW6Hz3AA&callback=myMap"></script> -->
<!DOCTYPE html>
<html lang="zxx">
<style>
    @media(max-width:767px) {
        .webView {
            display: none;
        }

        .token-title,
        .token-title-active,
        .token-discount,
        .token-discount-active,
        .token-price {
            font-size: 12px;
        }

        .bg-circle {
            display: none;
        }

        .token-card {
            width: 200px !important;
        }
    }

    @media(max-width:380px) {
        .token-card {
            width: 160px !important;
        }
    }

    @media(min-width:768px) {
        .mobileNav {
            display: none;
        }
    }

    .token-card {
        width: 280px;
    }

    .nav-item .nav-menu li a {
        color: black !important;
    }

    .line-bottom {
        height: 4px;
        background-color: #008D50;
        width: 70%;
        margin-left: auto;
        margin-right: auto;
    }

    .line-bottom-premium {
        height: 4px;
        background-color: goldenrod;
        width: 40%;
        margin-left: auto;
        margin-right: auto;
    }

    .login-text {
        font-family: 'Roboto' !important;
        font-size: 16px;
        color: #272727;
    }

    .footer-stay {
        position: absolute;
        bottom: 0;
        width: 100%;
    }

    .text-center {
        font-family: 'Roboto' !important;
        font-size: 20px;
        font-weight: bold;
        color: #3A595D;
    }


    .fixed-top-custom {
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 1030;
    }

    .pointer {
        cursor: pointer;
    }

    .dropdown-list:hover {
        background-color: #D9FCFF;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        width: max-content;
        right: 0;
        /*z-index: 1;*/
    }

    .dropdown-content a {
        color: black;
        padding: 6px;
        text-align: left;
        text-decoration: none;
        display: block;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        padding: 12px 16px;
        z-index: 1;
        font-size: small;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown-content a:hover {
        color: #27AE60;
    }

    /* #submit{
    width: 247px;
    height: 54px;
    border-radius: 30px;
    align-content: center;
    background-color: #27AE60;
    color: white;
    margin-left: auto; 
    margin-right: auto;
} */


    html * {
        font-family: 'Roboto' !important;
    }

    .text {
        font-family: 'Times New Roman' !important;
    }

    i {
        font-family: "FontAwesome" !important;

        display: inline-block;
        font-style: normal;
        font-variant: normal;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
    }

    .bg-token-active {
        background-color: #3a595d;
    }

    .bg-token {
        background-color: #cecece;
    }

    .token-title-active {
        color: white;
    }

    .token-title {
        color: black;
    }

    .token-discount-active {
        color: #ffffff7b;
        margin-right: 10px;
    }

    .token-discount {
        color: #27ae60;
        margin-right: 10px;
    }

    .modal-header {
        border: none !important;
    }

    .bg-circle {
        height: 40px;
        width: 40px;
        border-radius: 50%;
        background-color: white;
        margin-left: 12px;
    }

    .hide {
        display: none;
    }

    .white-14 {
        color: white;
        font-size: 14px;
    }

    .black-bold {
        color: black;
        font-weight: bold;
        font-size: 14px;
    }

    .gray-bold {
        color: lightgray;
        font-weight: bold;
    }

    .overflow-hidden {
        overflow: hidden;
    }
</style>

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Waste 2 Wealth</title>
    <link rel="icon" type="image/png" sizes="16x16" href="img/website-logo.png">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body style="position: relative; height: auto; min-height: 100vh;">
    <!-- Page Preloder -->
    <!-- <div id="preloder">
        <div class="loader"></div>
    </div> -->

    <div id="body1">
        <div class="mobileNav fixed-top shadow-1" style="background-color: white">
            <!--Navbar-->
            <nav class="navbar navbar-light light-blue lighten-4">
                <!-- Navbar brand -->
                <button onclick="toggleSideMenu()" class="navbar-toggler toggler-example mr-2" type="button" style="height: 38px;"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button>
                <div class="logo" style="margin-right: 56px;">
                    <a href="./">
                        <img src="img/website-logo.png" style="width: 76px;">
                    </a>
                </div>
                <div class="row">
                    <div class="user hide">
                        <div class="dropdown my-auto mr-3">
                            <!-- <button class="btn" style="display: flex; color: white; font-size: 14px; padding: 0"><i style="font-size: 40px; margin-left: 5px; color: gray; background-color: white; border-radius: 50%;" class="fa fa-user-circle"></i></button> -->

                            <!-- <img id="previewProfile1" src="./img/avatar.svg" style="height: 38px; width: 38px; border-radius: 50%; object-fit: cover;"> -->

                            <div class="dropdown-content" style="border-radius: 4px; background-color: white; padding: 0">
                                <div style="padding: 10px;">
                                    <div onclick="window.location.href='./my-ads.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                        My Ads
                                    </div>
                                    <div onclick="window.location.href='./profile.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                        My Account
                                    </div>
                                    <div onclick="window.location.href='./token-list.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                        Buy Token
                                    </div>
                                </div>
                                <div style="background-color: #F2F2F2;">
                                    <div class="d-flex" style="padding: 10px 10px 10px 20px;">
                                        <div class="my-auto" style="color: #049D19; font-size: 14px;">
                                            <span id="userBalanceToken">0</span> Tokens
                                        </div>
                                        <div class="ml-5">
                                            <button onclick="window.location.href='./logout.php'" class="btn" style="background-color: white; border: 1px solid #CBCBCB; color: #5482FF;">Logout</button>
                                        </div>
                                    </div>
                                    <!-- <div onclick="window.location.href='./logout.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
    Logout
</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <button class="navbar-toggler toggler-example mr-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i style="font-size: 1em;" class="fas fa-bars fa-1x "></i></span></button> -->
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                    <ul class="navbar-nav">
                        <li class="nav-item justify-content-center">
                            <div class="d-flex my-auto">

                                <div class="guest my-auto">

                                    <a href="./signin.html"><button class="btn nav-title">Sign
                                            In</button></a>
                                    <span style="font-size: 14px; color: darkgoldenrod">or</span>
                                    <div class="dropdown">
                                        <button class="btn nav-title" style="display: flex;">Register
                                            <i style="font-size: 10px; margin-top: 4px; margin-left: 5px" class="fa fa-chevron-down"></i></button>
                                        <div class="dropdown-content" style="border-radius: 4px; padding: 10px; background-color: white">
                                            <div onclick="window.location.href='register-individu.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Register as Individual
                                            </div>
                                            <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                            </div>
                                            <div onclick="window.location.href='register-company.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Register as Company
                                            </div>
                                            <!-- <a href="#">English</a>
    <a href="#">Malay</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="./setup-ads.html"><button class="btn btn-primary mr-1" style="font-size: 12px; font-weight: bold;">Create
                                    Ads</button></a>
                        </li>

                        <li class="nav-item justify-content-center">

                        </li>
                        <li class="nav-item active justify-content-center">
                            <a class="nav-link" onclick="window.location.href='./supply.php'" href="#">Available Product <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" onclick="window.location.href='./demand.php'" href="#">Wanted Product</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link contactUs" href="#"> Contact Us</a>
                        </li>
                    </ul>
                    <!-- Links -->

                </div>
                <!-- Collapsible content -->

            </nav>
            <!--/.Navbar-->
            <div class="sideMenu hide d-flex" style="position: fixed; z-index: 100; width: 100%;">
                <div style="height: 100vh; width: 230px; min-width: 230px; overflow-y: auto; background-color: white; box-shadow: 0px 4px 8px 0px;">
                    <!-- head -->
                    <div style="width: 100%; background-color: #3A595D; padding: 12px;">
                        <div class="float-right">
                            <i onclick="toggleSideMenu()" class="fa fa-times" style="font-size: 20px; color: white; -webkit-text-stroke: 2px; -webkit-text-stroke-color: #3A595D; opacity: 0.6;"></i>
                        </div>
                        <div style="height: 24px; width: 100%;"></div>
                        <div class="user hide">
                            <div>
                                <img id="previewProfile1" src="./img/avatar.svg" style="height: 50px; width: 50px; border-radius: 50%; object-fit: cover;">
                            </div>
                            <div class="mt-2 white-14 d-flex justify-content-between">
                                <span class="navUserName"></span><span class="white-14" style="font-size: 14px;"><span class="navUserToken"></span> Token(s)</span>
                            </div>
                            <div class="navUserType gray-bold" style="font-size: 12px;"></div>
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                            </div>
                        </div>
                        <div class="guest">
                            <div onclick="window.location.href=`./signin.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                                Sign In
                            </div>
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                            </div>
                            <div class="white-14 d-flex justify-content-between myaccount" style="padding-top: 5px; padding-bottom: 5px;">
                                Register <span><i class="myaccount-icon fa fa-chevron-down"></i></span>
                            </div>
                            <div class="myaccount-child hide" style="font-size: 14px; color: white;">
                                <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                                <div onclick="window.location.href=`./register-individu.html`" class="mb-1 mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                    As Individual
                                </div>
                                <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                                <div onclick="window.location.href=`./register-company.html`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                    As Company
                                </div>
                            </div>
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                            </div>
                        </div>
                        <div onclick="window.location.href=`./setup-ads.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                            Create Ads <span><i class="fa fa-plus-circle"></i></span>
                        </div>
                        <div class="user hide">
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                            </div>
                            <div onclick="window.location.href=`./my-ads.html`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                                My Ads
                            </div>
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                            </div>
                            <div class="white-14 d-flex justify-content-between myaccount" style="padding-top: 5px; padding-bottom: 5px;">
                                My Account <span><i class="myaccount-icon fa fa-chevron-down"></i></span>
                            </div>
                            <div class="myaccount-child hide" style="font-size: 14px; color: white;">
                                <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                                <div onclick="window.location.href=`./profile.html`" class="mb-1 mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                    Profile
                                </div>
                                <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                                <div onclick="window.location.href=`./wallet.html`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                    My Wallet
                                </div>
                                <div style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;"></div>
                                <div onclick="window.location.href=`./logout.php`" class="mt-2 ml-3" style="padding-top: 5px; padding-bottom: 5px;">
                                    Logout
                                </div>
                            </div>
                            <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.5;">
                            </div>
                            <div onclick="window.location.href=`./token-list.php`" class="white-14 d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px;">
                                Buy Token
                            </div>
                        </div>
                    </div>
                    <div style="padding: 12px;">
                        <div onclick="window.location.href=`./supply.php`" class="d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                            Available Product
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.3;"></div>
                        <div onclick="window.location.href=`./demand.php`" class="d-flex justify-content-between" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                            Wanted Product
                        </div>
                        <div class="mt-2 mb-2" style="height: 1px; width:100%; background-color: darkgray; opacity: 0.3;"></div>
                        <div class="d-flex justify-content-between contactUs" style="padding-top: 5px; padding-bottom: 5px; font-size: 14px; color: gray;">
                            Contact Us
                        </div>
                    </div>
                </div>
                <div onclick="toggleSideMenu()" style="background-color: black; height: 100vh; width: 100%; opacity: 0.5;">

                </div>
            </div>
        </div>

        <div class="webView fixed-top">
            <div class="d-flex flex-fill">
                <div class="shadow-1" style="background-color: white; width: 100%; padding-top: 5px; padding-bottom: 5px;">
                    <div class="col-md-10 offset-md-1 " style="padding-top: 5px; padding-bottom: 5px;">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-between">
                                <div class="logo" style="width: 76px;">
                                    <a href="./">
                                        <img src="img/website-logo.png">
                                    </a>
                                </div>
                                <div class="d-flex ml-3">
                                    <div onclick="window.location.href='./supply.php'" class="nav-title my-auto ml-3 pointer">
                                        Available Product
                                    </div>
                                    <div onclick="window.location.href='./demand.php'" class="nav-title my-auto ml-3 pointer">
                                        Wanted Product
                                    </div>
                                    <div class="nav-title my-auto ml-3 pointer contactUs">
                                        Contact Us
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex my-auto">
                                <div class="guest my-auto">
                                    <a href="./signin.html"><button class="btn nav-title">Sign
                                            In</button></a>
                                    <span style="font-size: 14px; color: darkgoldenrod">or</span>
                                    <div class="dropdown">
                                        <button class="btn nav-title" style="display: flex;">Register
                                            <i style="font-size: 10px; margin-top: 4px; margin-left: 5px" class="fa fa-chevron-down"></i></button>
                                        <div class="dropdown-content" style="border-radius: 4px; padding: 10px; background-color: white">
                                            <div onclick="window.location.href='register-individu.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Register as Individual
                                            </div>
                                            <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                            </div>
                                            <div onclick="window.location.href='register-company.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Register as Company
                                            </div>
                                            <!-- <a href="#">English</a>
                                    <a href="#">Malay</a> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="user hide">
                                    <div class="dropdown my-auto mr-3">
                                        <!-- <button class="btn" style="display: flex; color: white; font-size: 14px; padding: 0"><i style="font-size: 40px; margin-left: 5px; color: gray; background-color: white; border-radius: 50%;" class="fa fa-user-circle"></i></button> -->
                                        <div>
                                            <img id="previewProfile" src="./img/avatar.svg" style="height: 42px; width: 42px; border-radius: 50%; object-fit: cover;">
                                        </div>
                                        <div class="dropdown-content" style="border-radius: 4px; background-color: white; padding: 0">
                                            <div style="padding: 10px;">
                                                <div onclick="window.location.href='./my-ads.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                    My Ads
                                                </div>
                                                <div onclick="window.location.href='./profile.html'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                    My Account
                                                </div>
                                                <div onclick="window.location.href='./token-list.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                    Buy Token
                                                </div>
                                                <!-- <div style="height: 1px; background-color: lightgray; width: 100%; margin-top: 5px; margin-bottom: 5px;">

                                            </div> -->
                                            </div>
                                            <div style="background-color: #F2F2F2;">
                                                <div class="d-flex" style="padding: 10px 10px 10px 20px;">
                                                    <div class="my-auto" style="color: #049D19; font-size: 14px;">
                                                        <span id="userBalanceToken1">0</span> Tokens
                                                    </div>
                                                    <div class="ml-5">
                                                        <button onclick="window.location.href='./logout.php'" class="btn" style="background-color: white; border: 1px solid #CBCBCB; color: #5482FF;">Logout</button>
                                                    </div>
                                                </div>
                                                <!-- <div onclick="window.location.href='./logout.php'" class="pointer dropdown-list" style="padding: 10px 10px 10px 10px;border-radius: 4px; font-size: 16px">
                                                Logout
                                            </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <a href="./login.html"><button class="btn"
                                style="color: white; font-size: 14px;">Register</button></a> -->
                                <a href="./setup-ads.html"><button class="btn btn-primary" style="font-size: 14px; font-weight: bold;padding:10px">Create
                                        Ads</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin: 0">
        <div class="col-6 offset-1 col-10" style="margin-top: 105px;">
            <div style="font-weight: bold; font-size: 34px;">
                Buy Tokens
            </div>
            <div style="margin-top: 24px;">
                To select package for your ads, you must have sufficient amount of tokens. Each package will cost
                different value of tokens.Therefore, you must purchase the tokens offered according to your needs.
            </div>
            <div style="margin-top: 20px; font-style: italic; font-size: 12px; color: #D91F1F; text-align: left;">
                *All prices shown are the original price.
            </div>
            <div style="margin-top: 24px;">
                <div class="d-flex" style="margin-bottom: 20px;">
                    <div class="my-auto" style="margin-right: 5px;">
                        <input type="radio" id="tokenOther" name="radioToken" class="pointer" checked>
                    </div>
                    <div id="backgroundOther" class="bg-token-active d-flex justify-content-between" style="height: 60px; padding: 1px; border-radius: 2px;">
                        <div class="d-flex token-card">
                            <div class="my-auto bg-circle">
                            </div>
                            <div id="titleOther" class="token-title-active my-auto ml-2">
                                Other
                            </div>
                            <div class="my-auto ml-2 d-flex flex-fill" style="background-color: white; margin-right: 10px;">
                                <input id="other" type="text" class="btn-block form-control" value="1" style="border: none" size="14">
                            </div>
                        </div>
                        <div class="d-flex" style="background-color:white; width: 85px;">
                            <div class="mx-auto my-auto token-price" style="color: #4b4b4b">
                                RM2.00
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tokenBody">
                </div>
                <div style="margin-top: 43px;">
                    <table>
                        <tr style="font-size: 16px;">
                            <td>Original Price:</td>
                            <td>RM<span id="subtotal">0.00</span></td>
                        </tr>
                        <tr style="font-size: 14px; color: gray">
                            <td>Discounted Price:</td>
                            <td>RM<span id="discountPrice">0.00</span></td>
                        </tr>
                        <tr style="color: #27ae60; font-size: 24px">
                            <td>Net Price:</td>
                            <td>RM<span id="total">0.00</span></td>
                        </tr>
                    </table>
                    <div style="margin-top: 30px; margin-bottom: 40px;">
                        <div class="d-flex pointer" style="width: 188px; height: 40px; ">
                            <button id="payBtn" class="btn btn-primary btn-block">Pay</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 
    <div class="col-md-12" style="margin-top: 93px">
        <div class="col-md-10 offset-md-1">
            <div class="col-md-10 mx-auto">
                <div class="col-md-10 mx-auto">
                    <div>
                        <a href="./news-list.html"
                            style="color: #0099FF; text-decoration: underline; margin-bottom: 10px;">
                            <- Back to news list</a> </div> <div id="published"
                                style="font-size: 14px; color: #C4C4C4; margin-bottom: 10px;">
                    </div>
                    <div id="content">
                        <div id="spinner" style="margin-left: 40%; margin-top: 50px; margin-bottom: 160px;">
                            <button class="btn" disabled>
                                <span class="spinner-border spinner-border-sm"></span>
                                Loading..
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div> -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center">
                        <p>Are you sure you want to buy the tokens?</p>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 50px; margin-bottom: 50px;">
                        <div style="width: 155px;">
                            <button id="buyBtn" class="btn btn-primary btn-block">Buy</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div id="body2" style="display: none;">
        <div style="width: 100%; height: 100vh; background-color: white;">
            <div class="d-flex justify-content-center" style="height: 100%; text-align: center;">
                <div class="my-auto">
                    <div>
                        <img src="./img/checkijau.svg">
                    </div>
                    <div style="color: #27AE60; font-size: 28px; margin-top: 20px;">
                        Successful!
                    </div>
                    <div style="font-weight: bold; font-size: 20px; margin-top: 40px">
                        You have paid for the tokens!
                    </div>
                    <div style="color: #707070; font-size: 18px; margin-top: 20px">
                        Your remaining tokens is: <span id="remainingToken"></span>
                    </div>
                    <div style="margin-top: 40px">
                        <button id="continueBtn" class="btn btn-success">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="body3" style="display: none;">
        <div style="width: 100%; height: 100vh; background-color: white;">
            <div class="d-flex justify-content-center" style="height: 100%; text-align: center;">
                <div class="my-auto">
                    <div>
                        <img src="./img/rejected.svg">
                    </div>
                    <div style="color: #F44336; font-size: 28px; margin-top: 20px;">
                        Payment Unsuccessful.
                    </div>
                    <div style="color: #707070; font-size: 18px; margin-top: 40px">
                        Sorry, your purchase could not be processed.
                    </div>
                    <div style="margin-top: 50px">
                        <button id="backBtn" class="btn btn-primary">Back to Buy Tokens</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- contact us Modal -->
    <div class="modal fade" id="contactUsModal" tabindex="-1" role="dialog" aria-labelledby="contactUsModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div style="text-align: center; font-size: 30px; margin-top: 10px;">Contact Us</div>
                    <div style="padding: 60px;">
                        <div id="successMsg" class="hide">
                            <div style="font-size: 14px; color: #269635;">
                                Thank you for your message. It has been sent!
                            </div>
                            <br>
                            <div>
                                <button onclick="$('#contactUsModal').modal('hide')" class="btn btn-block" style="background-color: #27AE60; border: 1px solid #27AE60; color: white;">Okay</button>
                            </div>
                        </div>
                        <div id="defaultContact">
                            <div>
                                <input id="contactUsName" type="text" class="form-control" placeholder="Name…">
                            </div>
                            <br>
                            <div>
                                <input id="contactUsEmail" type="email" class="form-control" placeholder="someone@example.com">
                            </div>
                            <br>
                            <div>
                                <select name="contactUsRegarding" id="contactUsRegarding" class="form-control">
                                    <option value="nothing">Regarding...</option>
                                    <option value="support">Support</option>
                                    <option value="report">Report</option>
                                </select>
                            </div>
                            <br>
                            <div>
                                <textarea id="contactUsMessage" class="form-control" cols="20" rows="7"></textarea>
                            </div>
                            <br>
                            <div>
                                <button id="sendContactUs" class="btn btn-block" style="background-color: #27AE60; border: 1px solid #27AE60; color: white;">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>


<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery.dd.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>

<script>
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    String.prototype.replaceAt = function(index, replacement) {
        return this.substr(0, index) + replacement + this.substr(index + replacement.length);
    }

    $(window).resize(function() {
        console.log('resize called');
        var width = $(window).width();
        var height = $(window).height();
        var newHeight = height - 60;

        $('.sideMenu').children('div').css('height', newHeight);

        if (width <= 768) {
            // $('.sideMenu').removeClass('hide');
        } else {
            $('.sideMenu').addClass('hide');
        }
    }).resize()

    $(document).ready(function() {
        $tokenList = [];
        $subTotalPrice = 0;
        $totalPrice = 0;
        $selectedToken = '';
        $quantity = 0;

        $user = null;
        getUser();
        getTokenList();
        getTokenBalance();
        $('#spinner').css('display', 'none');

        $("#other").inputFilter(function(value) {
            return /^\d*$/.test(value); // Allow digits only, using a RegExp
        });

        $('#payBtn').on('click', function() {
            console.log($totalPrice);
            $('#exampleModalCenter').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $('#buyBtn').on('click', function() {
            $('#buyBtn').attr('disabled', true);
            $postData = {
                'user_id': $user['user_id'],
                'netprice': $totalPrice,
                'quantitytoken': $quantity,
            };

            console.log($postData);

            buyToken($postData);
            // $('#body3').css('display', 'none');
        });

        $('#continueBtn').on('click', function() {
            // $('#body1').css('display', 'block');
            // $('#body2').css('display', 'none');
            window.location.reload();
        });

        $('#backBtn').on('click', function() {
            // $('#body1').css('display', 'block');
            // $('#body3').css('display', 'none');
            window.location.reload();
        });

        $('.contactUs').on('click', function() {
            drawContactUs($user);
            $('#contactUsModal').modal();
        });
        $('#contactUs1').on('click', function() {
            drawContactUs($user);
            $('#contactUsModal').modal();
        });


        $('#sendContactUs').on('click', function() {
            var name = '';
            var email = '';
            var category = '';
            var message = '';
            name = $('#contactUsName').val();
            email = $('#contactUsEmail').val();
            category = $('#contactUsRegarding').val();
            message = $('#contactUsMessage').val();
            if (name != '' && email != '' && category != 'nothing' && message != '') {
                $postData = {
                    'name': name,
                    'email': email,
                    'category': category,
                    'description': message,
                };
                postContactUs($postData);
            }
        });

        $("#contactUsModal").on("hidden.bs.modal", function() {
            $('#contactUsName').val('');
            $('#contactUsEmail').val('');
            $('#contactUsRegarding').val('nothing');
            $('#contactUsMessage').val('');
            $('#successMsg').addClass('hide');
            $('#defaultContact').removeClass('hide');
            $('#sendContactUs').attr('disabled', false);
        });

        $('.myaccount').on('click', function() {
            $('.myaccount-child').toggleClass('hide');

            if ($('.myaccount-icon').hasClass('fa-chevron-down')) {
                $('.myaccount-icon').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            } else {
                $('.myaccount-icon').addClass('fa-chevron-down').removeClass('fa-chevron-up');
            }
        });
    });

    function postContactUs(postData) {
        $('#sendContactUs').attr('disabled', true);
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/enquiry',
            method: 'POST',
            data: postData,
        }).done(function(response) {
            $('#defaultContact').addClass('hide');
            $('#successMsg').removeClass('hide');
            console.log(response);
        });
    }

    function getTokenList() {
        // $('#other').attr('disabled', true);
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/token',
        }).done(function(response) {
            console.log(response);
            $tokenBody = $('#tokenBody');
            $tokenBody.html('');
            if (response['status'] == 'success') {
                if (response['value'].length != 0) {
                    $tokenList = response['value'];
                    $('#subtotal').html('2.00');
                    $('#discountPrice').html('0.00');
                    $('#total').html('2.00');

                    $('#other').attr('disabled', false);
                    $quantity = $('#other').val();
                    var quantity = $('#other').val();
                    var unitPrice = 2;

                    $subTotalPrice = quantity * unitPrice;
                    $totalPrice = $subTotalPrice;

                    $('#subtotal').html($subTotalPrice.toFixed(2));
                    $('#total').html($totalPrice.toFixed(2));

                    $('#other').keyup(function() {
                        var input = $('#other').val();

                        if (input.length > 3) {
                            console.log('lebih')
                            var outrangeNum = input.substring(0, 3);

                            $('#other').val(outrangeNum);
                        }

                        $quantity = $('#other').val();
                        var quantity = $quantity;
                        if (quantity != null || !quantity.contains('e')) {
                            $subTotalPrice = quantity * unitPrice;
                            $totalPrice = $subTotalPrice;
                        }
                        $('#subtotal').html($subTotalPrice.toFixed(2));
                        $('#total').html($totalPrice.toFixed(2));
                    });

                    $.each($tokenList, function(i, data) {
                        if (data['publish'] === 'yes') {
                            var div = '';
                            var backgroundClass = 'bg-token';
                            var tokenTitleClass = 'token-title';
                            var tokenDiscountClass = 'token-discount';
                            var discount = '';
                            if (data['discount'] == '0%') {
                                discount = '(Noble Price)';
                            } else {
                                discount = '(' + data['discount'] + '% Discount)';
                            }
                            div += '<div class="d-flex" style="margin-bottom: 20px;">';
                            div += '<div class="my-auto" style="margin-right: 5px;">';

                            div += '<input type="radio" id="' + i + '" name="radioToken" class="pointer">';
                            div += '</div>';
                            div += '<div id="background' + i + '" class="d-flex ' + backgroundClass + '" style="height: 60px; padding: 1px; border-radius: 2px;">';
                            div += '<div class="d-flex token-card">';
                            div += '<div class="my-auto bg-circle">';
                            div += '</div>';
                            div += '<div id="title' + i + '" class="my-auto ml-2 ' + tokenTitleClass + '">';
                            div += data['quantity'] + ' Tokens';
                            div += '</div>';
                            div += '<div id="discount' + i + '" class="my-auto ml-2 ' + tokenDiscountClass + '">';
                            div += discount;
                            div += '</div>';
                            div += '</div>';
                            div += '<div class="d-flex" style="background-color:white; width: 85px;">';
                            div += '<div class="mx-auto my-auto token-price" style="color: #4b4b4b">';
                            div += 'RM' + data['price'];
                            div += '</div>';
                            div += '</div>';
                            div += '</div>';
                            div += '</div>';

                            $tokenBody.append(div);
                        }
                    });
                    $('input[type=radio][name=radioToken]').change(function() {
                        var idToken = $(this)[0].id;

                        $subTotalPrice = 0.00;
                        $totalPrice = 0.00;

                        if (idToken == 'tokenOther') {
                            $('#other').attr('disabled', false);
                            var quantity = $('#other').val();
                            var unitPrice = 2;

                            $subTotalPrice = quantity * unitPrice;
                            $totalPrice = $subTotalPrice;

                            $('#subtotal').html($subTotalPrice.toFixed(2));
                            $('#total').html($totalPrice.toFixed(2));
                            $('#discountPrice').html('0.00');

                            $('#other').keyup(function() {
                                $quantity = $('#other').val();
                                var quantity = $quantity;
                                if (quantity != null || !quantity.contains('e')) {
                                    $subTotalPrice = quantity * unitPrice;
                                    $totalPrice = $subTotalPrice;
                                }
                                $('#subtotal').html($subTotalPrice.toFixed(2));
                                $('#total').html($totalPrice.toFixed(2));
                            });
                        } else {
                            $('#other').attr('disabled', true);
                            $subTotalPrice = parseFloat($tokenList[idToken]['price']);
                            var tempDiscount = $tokenList[idToken]['discount'];
                            var parsed = parseFloat(tempDiscount.split('%')[0]);
                            var discount = 1 - (parsed / 100);
                            $('#discountPrice').html(((1 - discount) * $subTotalPrice).toFixed(2));
                            $totalPrice = $subTotalPrice * discount;
                        }

                        $('#subtotal').html($subTotalPrice.toFixed(2));
                        $('#total').html($totalPrice.toFixed(2));

                        $.each($tokenList, function(i, data) {
                            if ($('#tokenOther').is(':checked')) {
                                $quantity = $('#other').val();
                                $('#backgroundOther').removeClass('bg-token').addClass('bg-token-active');
                                $('#titleOther').removeClass('token-title').addClass('token-title-active');
                            } else {
                                $('#backgroundOther').removeClass('bg-token-active').addClass('bg-token');
                                $('#titleOther').removeClass('token-title-active').addClass('token-title');
                            }

                            if ($('#' + i).is(':checked')) {
                                $quantity = data['quantity'];
                                $('#background' + i).removeClass('bg-token').addClass('bg-token-active');
                                $('#title' + i).removeClass('token-title').addClass('token-title-active');
                                $('#discount' + i).removeClass('token-discount').addClass('token-discount-active');
                            } else {
                                $('#background' + i).removeClass('bg-token-active').addClass('bg-token');
                                $('#title' + i).removeClass('token-title-active').addClass('token-title');
                                $('#discount' + i).removeClass('token-discount-active').addClass('token-discount');
                            }
                        });
                    });
                }
            }
        });
    }

    function tokenDetails(id) {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/token/detail?id=' + id,
        }).done(function(response) {

        });
    }

    function getTokenBalance() {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/token/mytoken?userid=' + $user['user_id'],
        }).done(function(response) {
            console.log(response);
            var data = response['value'];
            $('#userBalanceToken').html(data['tokenbalance']);
            $('.navUserToken').html(data['tokenbalance']);
        });
    }

    function getUser() {
        $user = JSON.parse(localStorage.getItem('w2wUser'));
        console.log($user);
        if ($user != null) {
            drawContactUs($user);
            var profilePicture = $user['profilepicture'];
            var extension = profilePicture.substr(profilePicture.length - 4);
            // if (extension.indexOf('png') >= 1 || extension.indexOf('jpg') >= 1 || extension.indexOf('jpeg') >= 1) {
            $('#previewProfile').attr('src', profilePicture);
            $('#previewProfile1').attr('src', profilePicture);

            if ($user['user_type'] != 'company') {
                $('.navUserName').html($user['user_fname']).css('text-transform', 'capitalize');
            } else {
                $('.navUserName').html($user['companyname']).css('text-transform', 'capitalize');
            }
            $('.navUserType').html($user['user_type']).css('text-transform', 'capitalize');
            // }
            if ($user['balancetoken'] != null) {
                $('#userBalanceToken').html($user['balancetoken']);
                $('#userBalanceToken1').html($user['balancetoken']);
                $('.navUserToken').html($user['balancetoken']);
            } else {
                $('#userBalanceToken').html('0');
                $('#userBalanceToken1').html('0');
                $('.navUserToken').html('0');
            }
            $('.guest').addClass('hide');
            $('.user').removeClass('hide');
        }
    }

    function drawContactUs(data) {
        if (data != '' && data != null) {
            if (data['user_type'] == 'company') {
                $('#contactUsName').val(data['companyname']);
            } else {
                $('#contactUsName').val(data['user_fname'] + ' ' + data['user_lname']);
            }
            $('#contactUsEmail').val(data['user_email']);
        }
    }

    function updateUser() {
        $.ajax({
            url: 'https://codeviable.com/w2w2/public/user/detail?user_id=' + $user['user_id'],
        }).done(function(response) {
            if (response['status'] == 'success') {
                localStorage.setItem('w2wUser', JSON.stringify(response['value']));
            }
        });
    }

    function buyToken(postData) {
        $.ajax({
            // url: 'https://codeviable.com/w2w2/public/token/addtoken',
            url: 'https://codeviable.com/testbillplz/public/token/buytoken',
            method: 'POST',
            data: postData,
        }).done(function(response) {
            $('#buyBtn').attr('disabled', false);
            console.log(response);
            window.location.href = response;
            // $('#exampleModalCenter').modal('hide');
            // $('#body1').css('display', 'none');
            // $('#body2').css('display', 'block');

            // var win = window.open();
            // var html = response;
            // win.document.write(html);
            // win.document.close();

            // $(win.document.body).text(html);
            // win.document.body.appendChild(html);

            // if (response['status'] == 'success') {
            //     var oldBalance = 0;
            //     if ($user['balancetoken'] != null) {
            //         oldBalance = parseInt($user['balancetoken']);
            //     }
            //     var addedToken = parseInt($quantity);
            //     var newBalance = oldBalance + addedToken;
            //     $('#remainingToken').html(newBalance);
            //     updateUser();
            // }
        });
    }

    function toggleSideMenu() {
        $('.sideMenu').toggleClass('hide');
        $('.sideMenu').children('div').scrollTop('0');
        $('.myaccount-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        $('.myinventory-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        $('.makeanoffer-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        $('.myaccount-child').addClass('hide');
        $('.myinventory-child').addClass('hide');
        $('.makeanoffer-child').addClass('hide');

        if ($('html, body').hasClass('overflow-hidden')) {
            $('html, body').removeClass('overflow-hidden');
        } else {
            $('html, body').addClass('overflow-hidden');
        }
    }
</script>